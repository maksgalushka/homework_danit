import { useEffect, useState } from 'react';
import Dashboard from './pages/dashboard/DashBoard';
import axios from 'axios';

function App() {

  const [customers, setCustomers] = useState([]);

  useEffect(() => {
    const url = "http://localhost:9000/customers";
    axios.get(url).then(response => {
      console.log('respone: ');
      console.log(response);
      setCustomers(response.data);
    }).catch(error => {
      console.log(error);
    })
  }, [])



  return (
    <Dashboard customers={customers} />
  );
}

export default App;
