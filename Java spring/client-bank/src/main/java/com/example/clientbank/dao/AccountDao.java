package com.example.clientbank.dao;

import com.example.clientbank.domain.Account;
import com.example.clientbank.domain.Currency;
import com.example.clientbank.domain.Customer;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class AccountDao implements Dao<Account>{
    List<Account> accounts = new ArrayList<>();

    @Override
    public Account save(Account account) {
        accounts.add(account);
        return account;
    }

    @Override
    public boolean delete(Account obj) {
        if (accounts.contains(obj)) {
            accounts.remove(obj);
            return true;
        }
        return false;
    }

    @Override
    public void deleteAll(List<Account> entities) {
        accounts.removeAll(entities);
    }

    @Override
    public void saveAll(List<Account> entities) {
        accounts.addAll(entities);
    }

    @Override
    public List<Account> findAll() {
        return accounts;
    }

    @Override
    public boolean deleteById(long id) {
        accounts.forEach(e -> {
            if (e.getId().equals(id)) {
                accounts.remove(e);
            }
        });
        return true;
    }

    @Override
    public Account getOne(long id) {
        return accounts.stream().filter(e -> e.getId().equals(id)).findFirst().get();
    }

    public Account getByAccountNumber(String accountNumber) {
        return accounts.stream().filter(e -> e.getNumber().equals(accountNumber)).findFirst().get();
    }
}
