package com.example.clientbank.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import java.util.UUID;

@Getter
@Setter
@ToString
public class Account {
    private static Long counter = 1L;
    private Long id;
    private final String number = UUID.randomUUID().toString();
    private Currency currency;
    private Double balance = 0D;
    @JsonIgnore
    private Customer customer;


    public Account(Currency currency, Customer customer) {
        this.currency = currency;
        this.customer = customer;
        this.id = counter++;
    }

    public void topUp(Double amount) {
        setBalance(balance + amount);
    }

    public boolean withdrawMoney(Double amount) {
        if(balance >= amount) {
            setBalance(balance - amount);
            return true;
        }
        return false;
    }

    public boolean transferMoney(Double amount, Account toAccount) {
        if(balance >= amount) {
            setBalance(balance - amount);
            toAccount.setBalance(toAccount.getBalance() + amount);
            return true;
        }
        return false;
    }

}
