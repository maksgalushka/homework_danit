package com.example.clientbank.domain;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class Customer {
    private static Long counter = 1L;
    private Long id;
    private String name;
    private String email;
    private Integer age;
    private List<Account> accounts = new ArrayList<>();

    public Customer(String name, String email, Integer age) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.id = counter++;
    }

    public Customer(String name, String email, Integer age, List<Account> accounts) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.accounts = accounts;
        this.id = counter++;
    }
}
