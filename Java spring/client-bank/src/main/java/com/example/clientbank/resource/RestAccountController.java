package com.example.clientbank.resource;

import com.example.clientbank.domain.Account;
import com.example.clientbank.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/accounts")
@Slf4j
@CrossOrigin(origins = {"http://localhost:3000"})
public class RestAccountController {
    @Autowired
    private final AccountService accountService;

    @GetMapping
    public List<Account> getAll() {return accountService.getAll();}

    @PutMapping("/top-up")
    public void topUpAccount(Long accountId, Double amount) {
        accountService.topUpAccount(accountService.getById(accountId), amount);
    }

    @PutMapping("/withdraw")
    public boolean withdrawMoney(Long accountId, Double amount) {
        if (accountService.withdrawMoney(accountService.getById(accountId), amount)) {
            return true;
        }
        log.error("Error");
        return false;
    }

    @PutMapping("/transfer")
    public boolean transferMoney(String numberAccountFrom, Double amount, String numberAccountTo) {
        if (accountService.transferMoney(numberAccountFrom, amount, numberAccountTo)) {
            return true;
        }
        log.error("Error");
        return false;
    }
}
