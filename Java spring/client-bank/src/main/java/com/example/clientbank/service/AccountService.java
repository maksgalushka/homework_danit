package com.example.clientbank.service;

import com.example.clientbank.domain.Account;
import com.example.clientbank.domain.Customer;

import java.util.List;

public interface AccountService {
    List<Account> getAll();

    Account getById(Long userId);

    void delete(long id );
    void create(Account employee);

    void topUpAccount(Account account, Double amount);

    boolean withdrawMoney(Account account, Double amount);

    boolean transferMoney(String numberAccountFrom, Double amount, String numberAccountTo);
}
