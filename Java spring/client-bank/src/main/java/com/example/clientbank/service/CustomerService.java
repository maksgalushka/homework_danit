package com.example.clientbank.service;

import com.example.clientbank.domain.Account;
import com.example.clientbank.domain.Currency;
import com.example.clientbank.domain.Customer;

import java.util.List;

public interface CustomerService {
    List<Customer> getAll();

    Customer getById(Long userId);

    void delete(long id );

    boolean update(Customer customer);
    void create(Customer employee);

    void createCustomerAccount (Long customerId, Currency currency);

    boolean deleteCustomerAccount (Customer customer, Account account);
}
