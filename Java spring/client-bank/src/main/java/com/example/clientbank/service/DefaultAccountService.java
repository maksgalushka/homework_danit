package com.example.clientbank.service;

import com.example.clientbank.dao.AccountDao;
import com.example.clientbank.domain.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultAccountService implements AccountService{
    @Autowired
    private AccountDao accountDao;

    @Override
    public List<Account> getAll() {
        return accountDao.findAll();
    }

    @Override
    public Account getById(Long userId) {
        return accountDao.getOne(userId);
    }

    @Override
    public void delete(long id) {
        accountDao.delete(accountDao.getOne(id));
    }

    @Override
    public void create(Account account) {
        accountDao.save(account);
    }

    @Override
    public void topUpAccount(Account account, Double amount) {
        account.topUp(amount);
    }

    @Override
    public boolean withdrawMoney(Account account, Double amount) {
        return account.withdrawMoney(amount);
    }

    @Override
    public boolean transferMoney(String numberAccountFrom, Double amount, String numberAccountTo) {
        Account accountFrom = accountDao.getByAccountNumber(numberAccountFrom);
        Account accountTo = accountDao.getByAccountNumber(numberAccountTo);
        return accountFrom.transferMoney(amount, accountTo);
    }
}
