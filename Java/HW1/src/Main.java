import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter your name:");
        String userName = scan.next();

        int randomNumber = (int) (Math.random() * 100);

        System.out.println("Let the game begin!");
        int userNumber = scan.nextInt();

        while(userNumber != randomNumber) {
            if (randomNumber > userNumber) {
                System.out.println("Your number is too small. Please, try again.");
                userNumber = scan.nextInt();
                continue;
            }
            System.out.println("Your number is too big. Please, try again.");
            userNumber = scan.nextInt();
        }
        System.out.println("Congratulations, " +userName+ "!");
    }
}