import human.Human;
import human.Man;
import human.Woman;
import org.mockito.cglib.core.Local;
import pet.Pet;
import pet.Dog;
import family.CollectionFamilyDao;
import family.Family;
import family.FamilyController;
import family.FamilyService;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class Main {
    public static void main(String[] args) {



        HashSet<String> petHabits = new HashSet<> ();
        petHabits.add("play dirty tricks");
        petHabits.add("eat a bone");
        Dog pet = new Dog( "jhon", 8, petHabits, 56);

        Woman mother = new Woman("March", "Simpson", "1982/12/02");

        Man father = new Man("Gamer", "Simpson", "1980/08/04");

        HashMap<String, String> childSchedule = new HashMap<>();
        childSchedule.put(DayOfWeek.MONDAY.name(), "task1");
        childSchedule.put(DayOfWeek.TUESDAY.name(), "task2");
        childSchedule.put(DayOfWeek.WEDNESDAY.name(), "task3");
        Man child = new Man("Bard", "Simpson", "2003/09/16", 70, childSchedule);

        ArrayList<Human> childList = new ArrayList<>();
        childList.add(child);
        HashSet<Pet> pets = new HashSet<>();
        pets.add(pet);

        Family fam1 = new Family(mother, father, childList, pets);
        mother.setFamily(fam1);
        father.setFamily(fam1);
        child.setFamily(fam1);

        Family fam2 = new Family(mother, father);

        ArrayList<Family> famList = new ArrayList<>();
        famList.add(fam1);
        famList.add(fam2);
        CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao(famList);
        FamilyService familyService = new FamilyService(collectionFamilyDao);
        FamilyController familyController = new FamilyController(familyService);
        System.out.println("--------------------------------displayAllFamilies--------------------------------");
        familyController.displayAllFamilies();
        System.out.println("--------------------------------getFamiliesBiggerThan--------------------------------");
        System.out.println(familyController.getFamiliesBiggerThan(2));
        System.out.println("---------------------------------getFamiliesLessThan---------------------------------");
        familyController.getFamiliesLessThan(3);
        System.out.println("----------------------------countFamiliesWithMemberNumber----------------------------");
        System.out.println(familyController.countFamiliesWithMemberNumber(3));
        System.out.println("-----------------------------------createNewFamily------------------------------------");
        Man father2 = new Man("father2Name", "father2Surname","1998/04/06");
        Woman mother2 = new Woman("mother2Name", "mother2Surname", "2000/05/27");
        familyController.createNewFamily(mother2, father2);
        familyController.displayAllFamilies();
        System.out.println("---------------------------------deleteFamilyByIndex----------------------------------");
        familyController.deleteFamilyByIndex(1);
        familyController.displayAllFamilies();
        System.out.println("--------------------------------------bornChild---------------------------------------");
        familyController.bornChild(familyController.getFamilyById(1), "Gena", "Liza");
        familyController.displayAllFamilies();
        System.out.println("--------------------------------------adoptChild---------------------------------------");
        Woman adoptedChild = new Woman("Kate", "Surname", "2013/08/04");
        System.out.println(familyController.adoptChild(familyController.getFamilyById(1), adoptedChild));
        System.out.println("-------------------------------deleteAllChildrenOlderThen-------------------------------");
        familyController.deleteAllChildrenOlderThen(13);
        familyController.displayAllFamilies();
        System.out.println("-----------------------------------------count-----------------------------------------");
        System.out.println(familyController.count());
        System.out.println("--------------------------------------getFamilyById-------------------------------------");
        System.out.println(familyController.getFamilyById(1));
        System.out.println("-----------------------------------------getPets----------------------------------------");
        System.out.println(familyController.getPets(0));
        System.out.println("-----------------------------------------addPet-----------------------------------------");
        System.out.println(familyController.getFamilyById(1).getPet());
        familyController.addPet(1, pet);
        System.out.println(familyController.getFamilyById(1).getPet());
        System.out.println("-----------------------------------------DATE-----------------------------------------");
        System.out.println(adoptedChild.describeAge());
        Runtime.getRuntime().gc();



    }
}