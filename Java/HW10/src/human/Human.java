package human;

import family.Family;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Objects;

public class Human {
    private String name="";
    private String surname="";
    private long birthDate;
    private int iq;
    private HashMap<String, String> schedule = new HashMap<>();
    private Family family;

    public Human() {
    }


    public Human(String name, String surname, String birthDate) {
        this.name = name;
        this.surname = surname;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        long birthDateMilli = LocalDate.parse(birthDate, formatter).atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();
        this.birthDate = birthDateMilli;
    }

    public Human(String name, String surname, LocalDate birthDate) {
        this.name = name;
        this.surname = surname;
        long birthDateMilli = birthDate.atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();
        this.birthDate = birthDateMilli;
    }

    public Human(String name, String surname, String birthDate, int iq, HashMap<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        long birthDateMilli = LocalDate.parse(birthDate, formatter).atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();
        this.birthDate = birthDateMilli;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Family getFamily() {return family;}
    public void setFamily(Family family) {this.family = family;}
    public String getFullName() {return name + " " + surname;}
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}
    public String getSurname() {return surname;}
    public void setSurname(String surname) {this.surname = surname;}
    public long getBirthDate() {return birthDate;}
    public void setBirthDate(String birthDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        long birthDateMilli = LocalDate.parse(birthDate, formatter).atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();
        this.birthDate = birthDateMilli;
    }
    public int getIq() {return iq;}
    public void setIq(int iq) {this.iq = iq;}
    public HashMap<String, String> getSchedule() {return schedule;}
    public void setSchedule(HashMap<String, String> schedule) {this.schedule = schedule;}

    public String describeAge() {
        LocalDate date = Instant.ofEpochMilli(this.birthDate).atZone(ZoneId.systemDefault()).toLocalDate();;
        return date.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate && iq == human.iq && name.equals(human.name) && surname.equals(human.surname) && Objects.equals(schedule, human.schedule) && Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, iq, schedule, family);
    }

    @Override
    public String toString() {
//        String schedule = "[";
//        for (int i = 0; i < this.schedule.length; i++) {
//            schedule += "[";
//            for (int j = 0; j < this.schedule[i].length; j++) {
//                    schedule += this.schedule[i][j];
//                if (!(this.schedule[i].length == j + 1)) {
//                    schedule += ", ";
//                }
//            }
//            schedule += "]";
//            if (!(this.schedule.length == i + 1)) {
//                schedule += ", ";
//            }
//        }
//        schedule += "]";

        return "Human.Human{name=" + this.getName() +
                ", surname=" + this.getSurname() +
                ", birthDate=" + this.describeAge() +
                ", iq=" + this.getIq() +
                ", schedule=" + schedule.toString() + "}";
    }


//    public void describePet() {
//        HashSet<Pet.Pet> pets = family.getPet();
//        String petTrickLevel = pet.getTrickLevel() > 50? "очень хитрый" : "почти не хитрый";
//        System.out.println("У меня есть " + pet.getSpecies() + ", ему " + pet.getAge() + " лет, он " + petTrickLevel + ".");
//    }


    @Override
    protected void finalize() {
        System.out.println("Human.Human{name:" + this.getName() +
                ", surname:" + this.getSurname());
    }
}

