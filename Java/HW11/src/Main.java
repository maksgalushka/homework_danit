import human.Human;
import human.Man;
import human.Woman;
import pet.Pet;
import pet.Dog;
import family.CollectionFamilyDao;
import family.Family;
import family.FamilyController;
import family.FamilyService;

import java.time.DayOfWeek;
import java.util.*;

public class Main {
    public static void main(String[] args) {

        CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao(new ArrayList<>());
        FamilyService familyService = new FamilyService(collectionFamilyDao);
        FamilyController familyController = new FamilyController(familyService);

        while(true) {
            System.out.println("- 1. Заполнить тестовыми данными \n" +
                    "- 2. Отобразить весь список семей\n" +
                    "- 3. Отобразить список семей, где количество людей больше заданного\n" +
                    "- 4. Отобразить список семей, где количество людей меньше заданного\n" +
                    "- 5. Подсчитать количество семей, где количество членов равно\n" +
                    "- 6. Создать новую семью\n" +
                    "- 7. Удалить семью по индексу семьи в общем списке\n" +
                    "- 8. Редактировать семью по индексу семьи в общем списке \n" +
                    "- 9. Удалить всех детей старше возраста");
            System.out.println("Впишите номер меню: ");
            Scanner scanner = new Scanner(System.in);
            try {
                int menuItem = scanner.nextInt();
                if (menuItem > 9 || menuItem < 1) {
                    throw new InputMismatchException();
                }

                switch (menuItem) {
                    case 1:
                        Woman mother = new Woman("March", "Simpson", "1982/12/02");
                        Man father = new Man("Gamer", "Simpson", "1980/08/04");
                        HashSet<String> petHabits = new HashSet<>();
                        petHabits.add("play dirty tricks");
                        petHabits.add("eat a bone");
                        Dog pet = new Dog("jhon", 8, petHabits, 56);
                        HashMap<String, String> childSchedule = new HashMap<>();
                        childSchedule.put(DayOfWeek.MONDAY.name(), "task1");
                        childSchedule.put(DayOfWeek.TUESDAY.name(), "task2");
                        childSchedule.put(DayOfWeek.WEDNESDAY.name(), "task3");
                        Man child = new Man("Bard", "Simpson", "2003/09/16", 70, childSchedule);
                        ArrayList<Human> childList = new ArrayList<>();
                        childList.add(child);
                        HashSet<Pet> pets = new HashSet<>();
                        pets.add(pet);
                        Family fam1 = new Family(mother, father, childList, pets);
                        mother.setFamily(fam1);
                        father.setFamily(fam1);
                        child.setFamily(fam1);
                        Man father2 = new Man("father2Name", "father2Surname", "1998/04/06");
                        Woman mother2 = new Woman("mother2Name", "mother2Surname", "2000/05/27");
                        Family fam2 = new Family(mother2, father2);
                        ArrayList<Family> famList = new ArrayList<>();
                        famList.add(fam1);
                        famList.add(fam2);
                        familyController.setFamilyList(famList);
                        break;
                    case 2:
                        ArrayList<Family> families = familyController.getAllFamilies();
                        if (families.size() == 0) {
                            System.out.println("Пока что нету семей.");
                        }
                        for (int i = 0; i < families.size(); i++) {
                            System.out.println((i + 1) + ": " + families.get(i));
                        }
                        break;
                    case 3:
                        System.out.println("Введите колличество людей:");
                        int numberBiggerThen = scanner.nextInt();
                        System.out.println(familyController.getFamiliesBiggerThan(numberBiggerThen));
                        break;
                    case 4:
                        System.out.println("Введите колличество людей:");
                        int numberLessThan = scanner.nextInt();
                        System.out.println(familyController.getFamiliesLessThan(numberLessThan));
                        break;
                    case 5:
                        System.out.println("Введите колличество людей:");
                        int numberFamMembers = scanner.nextInt();
                        System.out.println(familyController.countFamiliesWithMemberNumber(numberFamMembers));
                        break;
                    case 6:
                        System.out.println("Введите имя матери:");
                        String motherName = scanner.next();
                        System.out.println("Введите фамилию матери:");
                        String motherSurname = scanner.next();
                        System.out.println("Введите год рождения матери:");
                        String motherBirthYear = scanner.next();
                        System.out.println("Введите месяц рождения матери:");
                        String motherBirthMonth = scanner.next();
                        System.out.println("Введите день рождения матери:");
                        String motherBirthDate = scanner.next();
                        System.out.println("Введите iq матери:");
                        int motherIq = scanner.nextInt();
                        System.out.println("Введите имя отца:");
                        String fatherName = scanner.next();
                        System.out.println("Введите фамилию отца:");
                        String fatherSurname = scanner.next();
                        System.out.println("Введите год рождения отца:");
                        String fatherBirthYear = scanner.next();
                        System.out.println("Введите месяц рождения отца:");
                        String fatherBirthMonth = scanner.next();
                        System.out.println("Введите день рождения отца:");
                        String fatherBirthDate = scanner.next();
                        System.out.println("Введите iq отца:");
                        int fatherIq = scanner.nextInt();
                        String motherBirthday = motherBirthYear + "/" + motherBirthMonth + "/" + motherBirthDate;
                        String fatherBirthday = fatherBirthYear + "/" + fatherBirthMonth + "/" + fatherBirthDate;
                        Woman newMother = new Woman(motherName, motherSurname, motherBirthday, motherIq, new HashMap<>());
                        Man newFather = new Man(fatherName, fatherSurname, fatherBirthday, fatherIq, new HashMap<>());
                        familyController.createNewFamily(newFather, newMother);
                        break;
                    case 7:
                        System.out.println("Введите порядковый номер семьи (ID):");
                        int deletedChildFamId = scanner.nextInt();
                        familyController.deleteFamilyByIndex(deletedChildFamId);
                        break;
                    case 8:
                        System.out.println("  - 1. Родить ребенка\n" + "  - 2. Усыновить ребенка");
                        int addChildMenuItem = scanner.nextInt();
                        switch (addChildMenuItem) {
                            case 1:
                                System.out.println("Введите порядковый номер семьи (ID):");
                                int bornChildFamId = scanner.nextInt();
                                System.out.println("Введите какое имя дать мальчику:");
                                String maleName = scanner.next();
                                System.out.println("Введите какое имя дать девочке:");
                                String femaleName = scanner.next();
                                familyController.bornChild(familyController.getFamilyById(bornChildFamId), maleName, femaleName);
                                break;
                            case 2:
                                System.out.println("Введите порядковый номер семьи (ID):");
                                int adoptChildFamId = scanner.nextInt();
                                System.out.println("Введите пол ребенка (male/female):");
                                String gender = scanner.next();
                                if(!gender.equals("female") || !gender.equals("male")) throw new InputMismatchException();
                                System.out.println("Введите фамилию ребенка:");
                                String childSurname = scanner.next();
                                System.out.println("Введите имя ребенка:");
                                String childName = scanner.next();
                                System.out.println("Введите дату рождения ребенка:");
                                String childBirthday = scanner.next();
                                System.out.println("Введите IQ ребенка:");
                                int childIq = scanner.nextInt();
                                if (gender.equals("male")) {
                                    Man adoptedChild = new Man(childName, childSurname, childBirthday, childIq, new HashMap<>());
                                    familyController.adoptChild(familyController.getFamilyById(adoptChildFamId), adoptedChild);
                                } else if (gender.equals("female")) {
                                    Woman adoptedChild = new Woman(childName, childSurname, childBirthday, childIq, new HashMap<>());
                                    familyController.adoptChild(familyController.getFamilyById(adoptChildFamId), adoptedChild);
                                }
                                break;
                        }
                        break;
                    case 9:
                        System.out.println("Введите интересующий возраст:");
                        int childrenAge = scanner.nextInt();
                        familyController.deleteAllChildrenOlderThen(childrenAge);
                }
            } catch (InputMismatchException e){
                System.err.println("Некоректные данные! Попробуйте ещё раз.");
                continue;
            }
        }
    }
}