package human;

import java.time.LocalDate;
import java.util.HashMap;

final public class Woman extends Human{
    public Woman() {
    }

    public Woman(String name, String surname, String birthDate) {
        super(name, surname, birthDate);
    }

    public Woman(String name, String surname, LocalDate birthDate) {
        super(name, surname, birthDate);
    }

    public Woman(String name, String surname, String birthDate, int iq, HashMap<String, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

//    public void greetPet() {
//        System.out.println("Привет, " + this.getFamily().getPet().getNickname() + "! Это я - твоя хозяйка!");
//    }

    public void makeup() {
        System.out.println("Я крашусь");
    }

    @Override
    protected void finalize() {
        System.out.println("Human.Human{name:" + this.getName() +
                ", surname:" + this.getSurname());
    }
}
