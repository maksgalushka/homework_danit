package family;

import human.Human;
import human.Man;
import human.Woman;
import pet.Pet;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class FamilyService {
    CollectionFamilyDao collectionFamilyDao;

    public FamilyService(CollectionFamilyDao collectionFamilyDao) {
        this.collectionFamilyDao = collectionFamilyDao;
    }

    public ArrayList<Family> getAllFamilies() {
       return this.collectionFamilyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        System.out.println("FamList:");
        this.getAllFamilies()
                .stream()
                .forEach(el -> System.out.println(el));
    }

    public List<Family> getFamiliesBiggerThan(int number) {
        return this.getAllFamilies()
                .stream()
                .filter(e -> e.countFamily() > number)
                .collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int number) {
        return this.getAllFamilies()
                .stream()
                .filter(e -> e.countFamily() < number)
                .collect(Collectors.toList());
    }

    public int countFamiliesWithMemberNumber(int number) {
        return this.getAllFamilies()
                .stream()
                .filter(e -> e.countFamily() == number)
                .collect(Collectors.toList())
                .size();
    }

    public void createNewFamily(Human father, Human mother) {
        Family newFamily = new Family(father, mother);
        this.collectionFamilyDao.saveFamily(newFamily);
    }

    public void deleteFamilyByIndex(int index) {
         this.collectionFamilyDao.deleteFamily(index);
    }

    public void bornChild(Family currentFamily, String manName, String womanName) {
        int randomNumber = (int) (Math.random()*2);
        if (randomNumber == 0) {
            Woman child = new Woman(womanName, currentFamily.getFather().getSurname(), LocalDate.now());
            currentFamily.addChild(child, currentFamily);
        } else if (randomNumber == 1) {
            Man child = new Man(manName, currentFamily.getFather().getSurname(), LocalDate.now());
            currentFamily.addChild(child, currentFamily);
        }
        this.collectionFamilyDao.saveFamily(currentFamily);
    }

    public Family adoptChild(Family currentFamily, Human adoptedChild) {
        currentFamily.addChild(adoptedChild, currentFamily);
        return currentFamily;
    }

    public void deleteAllChildrenOlderThen(int age) {
        this.getAllFamilies()
                .stream()
                .forEach(family -> {
                    for (int j = 0; j < family.countFamily() - 2; j++) {
                        long longBrithDate = family.getChildren().get(j).getBirthDate();
                        int birthYear = Instant.ofEpochMilli(longBrithDate).atZone(ZoneId.systemDefault()).toLocalDate().getYear();
                        if (2023 - birthYear > age) {
                            family.deleteChild(family.getChildren().get(j));
                        }
                    }
                });
    }

    public int count() {
        return this.collectionFamilyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return this.collectionFamilyDao.getFamilyByIndex(index);
    }

    public HashSet<Pet> getPets(int index) {
        return this.getFamilyById(index).getPet();
    }

    public void addPet(int familyIndex, Pet pet) {
        this.getFamilyById(familyIndex).getPet().add(pet);
    }

    public void setFamilyList(ArrayList<Family> familyList) {
        this.collectionFamilyDao.setFamilyList(familyList);
    }
}
