package human;

import java.time.LocalDate;
import java.util.HashMap;

final public class Man extends Human implements java.io.Serializable{
    public Man() {
    }

    public Man(String name, String surname, String birthDate) {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, LocalDate birthDate) {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, String birthDate, int iq, HashMap<String, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

//    public void greetPet() {
//        System.out.println("Привет, " + this.getFamily().getPet().getNickname() + "! Это я - твой хозяин!");
//    }

    public void repairCar() {
        System.out.println("Я чиню машину!");
    }

    @Override
    protected void finalize() {
        System.out.println("Human.Human{name:" + this.getName() +
                ", surname:" + this.getSurname());
    }
}
