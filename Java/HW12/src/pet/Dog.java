package pet;

import java.util.HashSet;

public class Dog extends Pet implements FoulingPet, java.io.Serializable{

    public Dog() {}
    public Dog(String nickname, int age, HashSet<String> habits, int trickLevel) {
        super(nickname, age, habits);
        this.setSpecies(PetSpecies.DOG);
        this.setTrickLevel(trickLevel);
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.getNickname() + ". Я соскучился!");
    }
    @Override
    public String toString() {
        return this.getSpecies() +
                "{nickname=" + this.getNickname() +
                ", age=" + this.getAge() +
                ", trickLevel=" + this.getTrickLevel() +
                ", habits=" + this.getHabits().toString() + "}";
    }
}
