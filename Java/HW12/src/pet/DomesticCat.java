package pet;

import java.util.HashSet;

public class DomesticCat extends Pet implements java.io.Serializable {

    public DomesticCat(){}
    public DomesticCat(String nickname, int age, HashSet<String> habits, PetSpecies species, int trickLevel) {
        super(nickname, age, habits);
        this.setSpecies(PetSpecies.DOMESTIC_CAT);
        this.setTrickLevel(trickLevel);
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.getNickname() + ". Я соскучился!");
    }
    @Override
    public String toString() {
        return this.getSpecies() +
                "{nickname=" + this.getNickname() +
                ", age=" + this.getAge() +
                ", trickLevel=" + this.getTrickLevel() +
                ", habits=" + this.getHabits().toString() + "}";
    }
}
