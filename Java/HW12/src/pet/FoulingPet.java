package pet;

public interface FoulingPet {
    default void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

}
