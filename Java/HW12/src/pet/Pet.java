package pet;

import java.util.HashSet;

public abstract class Pet implements java.io.Serializable {
    private String nickname;
    private int age;
    private HashSet<String> habits = new HashSet<>();
    private int trickLevel;
    private PetSpecies species = PetSpecies.UNKNOWN;

    public Pet() {}

    public Pet(String nickname, int age, HashSet<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.habits = habits;
    }

    public PetSpecies getSpecies() {return species;}
    public void setSpecies(PetSpecies species) {this.species = species;}
    public int getTrickLevel() {return trickLevel;}
    public void setTrickLevel(int trickLevel) {this.trickLevel = trickLevel;}
    public String getNickname() {return nickname;}
    public void setNickname(String nickname) {this.nickname = nickname;}
    public int getAge() {return age;}
    public void setAge(int age) {this.age = age;}
    public HashSet<String> getHabits() {return habits;}
    public void setHabits(HashSet<String> habits) {this.habits = habits;}


    public void eat() { System.out.println("Я кушаю!"); }
    public abstract void respond();

    @Override
    protected void finalize() {
        System.out.println("Pet.Pet{nickname:" + this.getNickname() +
                ", age:" + this.getAge());
    }

}


