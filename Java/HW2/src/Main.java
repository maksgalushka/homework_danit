//import java.util.Arrays;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int randomColumn = (int)(Math.random()*5 + 1);
        int randomRow = (int)(Math.random()*5 + 1);
        System.out.println(randomColumn + " " + randomRow);

        String[][] matrix = {
                {"0", "1", "2", "3", "4", "5",},
                {"1", "-", "-", "-", "-", "-",},
                {"2", "-", "-", "-", "-", "-",},
                {"3", "-", "-", "-", "-", "-",},
                {"4", "-", "-", "-", "-", "-",},
                {"5", "-", "-", "-", "-", "-",},
        };

        System.out.println("All set. Get ready to rumble!");
        System.out.println("Enter your column(1-5):");
        int userColumn = scan.nextInt();
        while (userColumn > 5 || userColumn < 1) {
            System.out.println("Error, enter your column again:");
            userColumn = scan.nextInt();
        }

        System.out.println("Enter your row(1-5):");
        int userRow = scan.nextInt();
        while (userRow > 5 || userRow < 1) {
            System.out.println("Error, enter your row again:");
            userRow = scan.nextInt();
        }

        while(randomColumn != userColumn || randomRow != userRow) {

            matrix[userRow][userColumn] = "*";
            for (int i = 0; i < matrix.length; i++) {
                System.out.println("");
                for (int j = 0; j < matrix[i].length; j++) {
                    System.out.print(" " + matrix[i][j] + " |");
                }
            }
            System.out.println("");

            System.out.println("Enter your column(1-5):");
            userColumn = scan.nextInt();
            while (userColumn > 5 || userColumn < 1) {
                System.out.println("Error, enter your column again:");
                userColumn = scan.nextInt();
            }

            System.out.println("Enter your row(1-5):");
            userRow = scan.nextInt();
            while (userRow > 5 || userRow < 1) {
                System.out.println("Error, enter your row again:");
                userRow = scan.nextInt();
            }
        }
        matrix[randomRow][randomColumn] = "X";
        for (int i = 0; i < matrix.length; i++) {
            System.out.println("");
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(" " + matrix[i][j] + " |");
            }
        }
        System.out.println("");
        System.out.println("----------------------------");
        System.out.println("You have won!");
        System.out.println("----------------------------");

    }
}