import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String[][] schedule = new String[7][2];
        schedule[0][0] = "monday";
        schedule[1][0] = "tuesday";
        schedule[2][0] = "wednesday";
        schedule[3][0] = "thursday";
        schedule[4][0] = "friday";
        schedule[5][0] = "saturday";
        schedule[6][0] = "Sunday";

        schedule[0][1] = "Go shopping";
        schedule[1][1] = "Watch a film";
        schedule[2][1] = "Meet with friends";
        schedule[3][1] = "Do homework";
        schedule[4][1] = "Relax";
        schedule[5][1] = "Visit parents";
        schedule[6][1] = "Do some work";


        String userDay;
        outerLoop: do {
             System.out.println("Please, input the day of the week: ");
             userDay = scan.next().toLowerCase();

             for (int i = 0; i < 6; i++) {
                if (userDay.equals(schedule[i][0])) {
                    System.out.println("Your tasks for Monday: ");
                    for (int j = 1; j < schedule[i].length; j++) {
                        System.out.println(schedule[i][j] + ";");
                    }
                    continue outerLoop;
                }
             }
            System.out.println("Sorry, I don't understand you, please try again.");
        } while (!userDay.equals("exit".toLowerCase()));
        System.out.println("Have a nice day. Bye!");
    }
}