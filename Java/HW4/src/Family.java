import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children = {};
    private Pet pet;

    public Family() {
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public Family(Human mother, Human father, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.pet = pet;
    }

    public Family(Human mother, Human father, Human[] children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
    }

    public Family(Human mother, Human father, Human[] children, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pet = pet;
    }

    public Human getMother() {return mother;}
    public Human getFather() {return father;}
    public Human[] getChildren() {return children;}
    public void setChildren(Human[] children) {this.children = children;}
    public Pet getPet() {return pet;}
    public void setPet(Pet pet) {this.pet = pet;}

    public void addChild(Human newChild, Family family) {
        this.children = Arrays.copyOf(this.children, this.children.length + 1);
        this.children[this.children.length - 1] = newChild;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return mother.equals(family.mother) && father.equals(family.father) && Arrays.equals(children, family.children) && Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    public boolean deleteChild(Human deletedChild) {
        boolean isDeleted = false;
        for (int i = 0; i < this.getChildren().length; i++) {
            if (this.getChildren()[i].equals(deletedChild)) {
                Human[] updateChildren = new Human[this.getChildren().length - 1];
                for (int j = 0; j < this.getChildren().length; j++) {
                    if (this.getChildren()[j].equals(deletedChild)) {
                        this.getChildren()[j].setFamily(new Family());
                        isDeleted = true;
                    } else {
                        updateChildren[j] = this.getChildren()[j];
                    }
                }
                this.setChildren(updateChildren);
            }
        }

        return isDeleted;
    }

    public String childrenToString() {
        String children = "{";
        for (int i = 0; i < this.getChildren().length; i++) {
            children += this.getChildren()[i].toString() + ", ";
        }
        children += "}";
        return children;
    }

    @Override
    public String toString() {

        return "FAMILY:\nmother: " + mother.toString() +
                "\nfather: " + father.toString() +
                "\nchildren: " + this.childrenToString();
    }

    public int countFamily() {
        return 2 + this.getChildren().length;
    }

    @Override
    protected void finalize() {
        System.out.println(
                "FAMILY:\nmother: " + mother.toString() +
                "\nfather: " + father.toString() +
                "\nchildren: " + this.childrenToString()
        );
    }
}
