import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Pet pet = new Pet(PetSpecies.DOG, "jhon", 8, 56, new String[] {"play dirty tricks", "eat a bone"});
        pet.eat();
        pet.respond();
        pet.foul();
        System.out.println(pet.toString());
        System.out.println("--------------------------------------------------------------");


        Human mother = new Human("March", "Simpson", 1982);
        System.out.println(mother.toString());
        System.out.println("--------------------------------------------------------------");


        Human father = new Human("Gamer", "Simpson", 1980);
        System.out.println(father.toString());
        System.out.println("--------------------------------------------------------------");


        String childSchedule[][] = {
                {DayOfWeek.MONDAY.name(), "task1", "task2"},
                {DayOfWeek.TUESDAY.name(), "task3"},
                {DayOfWeek.WEDNESDAY.name(), "task4", "task5", "task6"}
        };
        Human child = new Human("Bard", "Simpson", 2003, 70, childSchedule);
        System.out.println(child.toString());
        System.out.println("--------------------------------------------------------------");


        Family fam1 = new Family(mother, father, new Human[] {child}, pet);
        mother.setFamily(fam1);
        father.setFamily(fam1);
        child.setFamily(fam1);
        child.greetPet();
        child.describePet();
        System.out.println(fam1.toString());
        System.out.println("Family size:" + fam1.countFamily());
        System.out.println("-------------------------DELETING CHILD-------------------------");
        fam1.deleteChild(child);
        System.out.println(fam1.toString());
        System.out.println("Family size:" + fam1.countFamily());
        System.out.println("-------------------------ADD CHILD-------------------------");
        fam1.addChild(child, fam1);
        System.out.println(fam1.toString());
        System.out.println("Family size:" + fam1.countFamily());
        System.out.println("--------------------------------------------------------------");
        System.out.println("--------------------------------------------------------------");
        System.out.println("--------------------------------------------------------------");

//        for (int i = 0; i < 10000; i++) {
//            Human hum = new Human("Human" + i, "Surname" + i, i);
//        }

        Runtime.getRuntime().gc();
    }
}