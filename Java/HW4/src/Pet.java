import java.util.Arrays;
import java.util.Objects;

public class Pet {
        private PetSpecies species;
        private String nickname;
        private int age;
        private int trickLevel;
        private String[] habits;

    public Pet() {

    }

    public Pet(PetSpecies species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet(PetSpecies species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

        public PetSpecies getSpecies() {return species;}
        public void setSpecies(PetSpecies species) {this.species = species;}
        public String getNickname() {return nickname;}
        public void setNickname(String nickname) {this.nickname = nickname;}
        public int getAge() {return age;}
        public void setAge(int age) {this.age = age;}
        public int getTrickLevel() {return trickLevel;}
        public void setTrickLevel(int trickLevel) {this.trickLevel = trickLevel;}
        public String[] getHabits() {return habits;}
        public void setHabits(String[] habits) {this.habits = habits;
        }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age && trickLevel == pet.trickLevel && Objects.equals(species, pet.species) && Objects.equals(nickname, pet.nickname) && Arrays.equals(habits, pet.habits);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(species, nickname, age, trickLevel);
        result = 31 * result + Arrays.hashCode(habits);
        return result;
    }

        public void eat() { System.out.println("Я кушаю!"); }
        public void respond() {
            System.out.println("Привет, хозяин. Я - " + this.nickname + ". Я соскучился!");
        }
        public void foul() {
            System.out.println("Нужно хорошо замести следы...");
        }

        @Override
        public String toString() {
            return this.getSpecies() +
                    "{nickname=" + this.getNickname() +
                    ", age=" + this.getAge() +
                    ", trickLevel=" + this.getTrickLevel() +
                    ", habits=" + Arrays.toString(this.getHabits()) + "}";
        }
}

enum PetSpecies  {
    DOG,CAT,PARROT,HAMSTER
}
