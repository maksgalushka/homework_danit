import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FamilyTest {
    private Family module;

    @BeforeEach
    public void setUp() {
        module = new Family();
    }

    @Test
    public void testToString() {
        String actual = module.toString();
        String expected = "FAMILY: null";
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testDeleteExistingChild() {
        Human child = new Human();
        Family module = new Family(null, null, new Human[] {child});
        module.deleteChild(child);
        int actual = module.getChildren().length;
        int expected = 0;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testDeleteNonExistingChild() {
        Human existingChild = new Human("name1", "lastname1", 2000);
        Human nonExistingChild = new Human("name2", "lastname2", 1000);
        Family module = new Family(null, null, new Human[] {existingChild});
        module.deleteChild(nonExistingChild);
        int actual = module.getChildren().length;
        int expected = 1;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testAddChild() {
        Human child = new Human();
        Family module = new Family(null, null);
        module.addChild(child, module);
        int actual = module.getChildren().length;
        int expected = 1;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testCountFamily() {
        Family module = new Family(null, null, new Human[] {null, null});
        int actual = module.countFamily();
        int expected = 4;
        Assertions.assertEquals(expected, actual);
    }


}
