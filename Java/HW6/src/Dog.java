import java.util.Arrays;
import java.util.Objects;

public class Dog extends Pet implements FoulingPet{

    public Dog() {}
    public Dog(String nickname, int age, String[] habits, int trickLevel) {
        super(nickname, age, habits);
        this.setSpecies(PetSpecies.DOG);
        this.setTrickLevel(trickLevel);
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.getNickname() + ". Я соскучился!");
    }
    @Override
    public String toString() {
        return this.getSpecies() +
                "{nickname=" + this.getNickname() +
                ", age=" + this.getAge() +
                ", trickLevel=" + this.getTrickLevel() +
                ", habits=" + Arrays.toString(this.getHabits()) + "}";
    }
}
