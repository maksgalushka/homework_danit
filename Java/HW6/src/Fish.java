import java.util.Arrays;
import java.util.Objects;

public class Fish extends Pet {

    public Fish(){}
    public Fish(String nickname, int age, String[] habits) {
        super(nickname, age, habits);
        this.setSpecies(PetSpecies.FISH);
    }


    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.getNickname() + ". Я соскучился!");
    }
    @Override
    public String toString() {
        return this.getSpecies() +
                "{nickname=" + this.getNickname() +
                ", age=" + this.getAge() +
                ", habits=" + Arrays.toString(this.getHabits()) + "}";
    }
}
