import java.util.Arrays;
import java.util.Objects;

public abstract class Pet {
    private String nickname;
    private int age;
    private String[] habits;
    private int trickLevel;
    private PetSpecies species = PetSpecies.UNKNOWN;

    public Pet() {}

    public Pet(String nickname, int age, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.habits = habits;
    }

    public PetSpecies getSpecies() {return species;}
    public void setSpecies(PetSpecies species) {this.species = species;}
    public int getTrickLevel() {return trickLevel;}
    public void setTrickLevel(int trickLevel) {this.trickLevel = trickLevel;}
    public String getNickname() {return nickname;}
    public void setNickname(String nickname) {this.nickname = nickname;}
    public int getAge() {return age;}
    public void setAge(int age) {this.age = age;}
    public String[] getHabits() {return habits;}
    public void setHabits(String[] habits) {this.habits = habits;}


    public void eat() { System.out.println("Я кушаю!"); }
    public abstract void respond();

    @Override
    protected void finalize() {
        System.out.println("Pet{nickname:" + this.getNickname() +
                ", age:" + this.getAge());
    }

}


