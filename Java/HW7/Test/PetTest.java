import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PetTest {
    private Pet module;

    @BeforeEach
    public void setUp() {
        module = new Dog();
    }

    @Test
    public void testToString() {
        String actual = module.toString();
        System.out.println(actual);
        String expected = "UNKNOWN{nickname=null, age=0, trickLevel=0, habits=[]}";
        Assertions.assertEquals(expected, actual);
    }
}
