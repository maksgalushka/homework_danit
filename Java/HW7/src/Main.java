import java.util.*;

public class Main {
    public static void main(String[] args) {

        HashSet<String> petHabits = new HashSet<> ();
        petHabits.add("play dirty tricks");
        petHabits.add("eat a bone");


        Dog pet = new Dog( "jhon", 8, petHabits, 56);
        pet.eat();
        pet.respond();
        pet.foul();
        System.out.println(pet.toString());
        System.out.println("--------------------------------------------------------------");


        Woman mother = new Woman("March", "Simpson", 1982);
        System.out.println(mother.toString());
        System.out.println("--------------------------------------------------------------");


        Man father = new Man("Gamer", "Simpson", 1980);
        System.out.println(father.toString());
        System.out.println("--------------------------------------------------------------");


        HashMap<String, String> childSchedule = new HashMap<>();
        childSchedule.put(DayOfWeek.MONDAY.name(), "task1");
        childSchedule.put(DayOfWeek.TUESDAY.name(), "task2");
        childSchedule.put(DayOfWeek.WEDNESDAY.name(), "task3");

        Man child = new Man("Bard", "Simpson", 2003, 70, childSchedule);
        System.out.println(child.toString());
        System.out.println("--------------------------------------------------------------");

        ArrayList<Human> childList = new ArrayList<>();
        childList.add(child);

        HashSet<Pet> pets = new HashSet<>();
        pets.add(pet);

        Family fam1 = new Family(mother, father, childList, pets);
        mother.setFamily(fam1);
        father.setFamily(fam1);
        child.setFamily(fam1);
//        child.greetPet();
//        child.describePet();
        System.out.println(fam1.toString());
        System.out.println("Family size:" + fam1.countFamily());
        System.out.println("-------------------------DELETING CHILD-------------------------");
        fam1.deleteChild(child);
        System.out.println(fam1.toString());
        System.out.println("Family size:" + fam1.countFamily());
        System.out.println("-------------------------ADD CHILD-------------------------");
        fam1.addChild(child, fam1);
        System.out.println(fam1.toString());
        System.out.println("Family size:" + fam1.countFamily());
        System.out.println("--------------------------------------------------------------");
        System.out.println("--------------------------------------------------------------");
        System.out.println("--------------------------------------------------------------");

//        for (int i = 0; i < 10000; i++) {
//            Human hum = new Human("Human" + i, "Surname" + i, i);
//        }

        Runtime.getRuntime().gc();
    }
}