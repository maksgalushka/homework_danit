import java.util.HashMap;

final public class Man extends Human{
    public Man() {
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, HashMap<String, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

//    public void greetPet() {
//        System.out.println("Привет, " + this.getFamily().getPet().getNickname() + "! Это я - твой хозяин!");
//    }

    public void repairCar() {
        System.out.println("Я чиню машину!");
    }

    @Override
    protected void finalize() {
        System.out.println("Human{name:" + this.getName() +
                ", surname:" + this.getSurname());
    }
}
