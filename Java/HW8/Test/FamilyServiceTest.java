import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class FamilyServiceTest {
    private FamilyController familyController;

    @BeforeEach
    public void setUp() {
        Dog pet = new Dog();
        Woman mother = new Woman("mother1", "motherSurname1", 1990);
        Man father = new Man("father1", "fatherSurname1", 1990);
        Man child = new Man("child1", "childSurname1", 2003);
        ArrayList<Human> childList = new ArrayList<>();
        childList.add(child);
        HashSet<Pet> pets = new HashSet<>();
        pets.add(pet);
        Family fam1 = new Family(mother, father, childList, pets);
        mother.setFamily(fam1);
        father.setFamily(fam1);
        child.setFamily(fam1);
        Family fam2 = new Family(mother, father);
        ArrayList<Family> famList = new ArrayList<>();
        famList.add(fam1);
        famList.add(fam2);
        CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao(famList);
        FamilyService familyService = new FamilyService(collectionFamilyDao);
        familyController = new FamilyController(familyService);
    }

    @Test
    public void testGetFamiliesBiggerThan() {
        int expected = 1;
        int actual = this.familyController.getFamiliesBiggerThan(2).size();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testGetFamiliesLessThan() {
        int expected = 1;
        int actual = this.familyController.getFamiliesLessThan(3).size();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testCountFamiliesWithMemberNumber() {
        int expected = 1;
        int actual = this.familyController.countFamiliesWithMemberNumber(2);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testCreateNewFamily() {
        int expected = 3;
        Man father = new Man();
        Woman mother = new Woman();
        this.familyController.createNewFamily(father, mother);
        int actual = this.familyController.count();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testDeleteFamilyByIndex() {
        int expected = 1;
        this.familyController.deleteFamilyByIndex(1);
        int actual = this.familyController.count();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testBornChild() {
        Family fam = this.familyController.getFamilyById(0);
        int expected = fam.getChildren().size() + 1;
        this.familyController.bornChild(fam, "Name1", "Name2");
        int actual = fam.getChildren().size();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testAdoptChild() {
        Family fam = this.familyController.getFamilyById(0);
        int expected = fam.getChildren().size() + 1;
        Man child = new Man();
        this.familyController.adoptChild(fam, child);
        int actual = fam.getChildren().size();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testDeleteAllChildrenOlderThen() {
        int expected = 0;
        Family fam = this.familyController.getFamilyById(0);
        this.familyController.deleteAllChildrenOlderThen(10);
        int actual = fam.getChildren().size();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testCount() {
        int expected = 2;
        int actual = this.familyController.count();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testGetFamilyById() {
        Family expected = this.familyController.getAllFamilies().get(0);
        Family actual = this.familyController.getFamilyById(0);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testGetPets() {
        int expected = 1;
        int actual = this.familyController.getPets(0).size();
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testAddPet() {
        int expected = this.familyController.getPets(0).size() + 1;
        this.familyController.addPet(0, new Dog());
        int actual = this.familyController.getPets(0).size();
        Assertions.assertEquals(expected, actual);
    }
}
