import java.util.HashMap;

final public class Woman extends Human{
    public Woman() {
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, HashMap<String, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

//    public void greetPet() {
//        System.out.println("Привет, " + this.getFamily().getPet().getNickname() + "! Это я - твоя хозяйка!");
//    }

    public void makeup() {
        System.out.println("Я крашусь");
    }

    @Override
    protected void finalize() {
        System.out.println("Human{name:" + this.getName() +
                ", surname:" + this.getSurname());
    }
}
