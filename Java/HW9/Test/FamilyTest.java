import human.Human;
import family.Family;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;

public class FamilyTest {
    private Family module;

    @BeforeEach
    public void setUp() {
        module = new Family();
    }

    @Test
    public void testToString() {
        String actual = module.toString();
        String expected = "FAMILY: null";
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testDeleteExistingChild() {
        Human child = new Human();
        ArrayList<Human> childList = new ArrayList<>();
        childList.add(child);
        Family module = new Family(null, null, childList);
        module.deleteChild(child);
        int actual = module.getChildren().size();
        int expected = 0;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testDeleteNonExistingChild() {
        Human existingChild = new Human("name1", "lastname1", "2000/01/01");
        Human nonExistingChild = new Human("name2", "lastname2", "2000/01/01");
        ArrayList<Human> childList = new ArrayList<>();
        childList.add(existingChild);
        Family module = new Family(null, null, childList);
        module.deleteChild(nonExistingChild);
        int actual = module.getChildren().size();
        int expected = 1;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testAddChild() {
        Human child = new Human();
        Family module = new Family(null, null);
        module.addChild(child, module);
        int actual = module.getChildren().size();
        int expected = 1;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testCountFamily() {
        ArrayList<Human> childList = new ArrayList<>();
        childList.add(null);
        childList.add(null);
        Family module = new Family(null, null, childList);
        int actual = module.countFamily();
        int expected = 4;
        Assertions.assertEquals(expected, actual);
    }


}
