package family;

import human.Human;
import pet.Pet;

import java.util.*;

public class Family{
    private Human mother;
    private Human father;
    private ArrayList<Human> children = new ArrayList<>();
    private HashSet<Pet> pets = new HashSet<>();

    public Family() {
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public Family(Human mother, Human father, HashSet<Pet> pets) {
        this.mother = mother;
        this.father = father;
        this.pets = pets;
    }

    public Family(Human mother, Human father, ArrayList<Human> children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
    }

    public Family(Human mother, Human father, ArrayList<Human> children, HashSet<Pet> pets) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pets = pets;
    }

    public Human getMother() {return mother;}
    public Human getFather() {return father;}
    public ArrayList<Human> getChildren() {return children;}
    public void setChildren(ArrayList<Human> children) {this.children = children;}
    public HashSet<Pet> getPet() {return pets;}
    public void setPet(HashSet<Pet> pets) {this.pets = pets;}

    public void addChild(Human newChild, Family family) {
        this.children.add(newChild);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return mother.equals(family.mother) && father.equals(family.father) && Objects.equals(children, family.children) && Objects.equals(pets, family.pets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, children, pets);
    }

    public boolean deleteChild(Human deletedChild) {
        this.children.remove(deletedChild);
        if (this.children.contains(deletedChild)) {
            return false;
        }
        return true;
    }

    public String childrenToString() {
        return this.children.toString();
    }

    @Override
    public String toString() {
        if (mother == null || father == null) {
            return "FAMILY: null";
        }

        return "FAMILY:\nmother: " + mother.toString() +
                "\nfather: " + father.toString() +
                "\nchildren: " + this.childrenToString();
    }

    public int countFamily() {
        return 2 + this.getChildren().size();
    }

    @Override
    protected void finalize() {
        System.out.println(
                "FAMILY:\nmother: " + mother.toString() +
                "\nfather: " + father.toString() +
                "\nchildren: " + this.childrenToString()
        );
    }
}
