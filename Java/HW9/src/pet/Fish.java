package pet;

import java.util.HashSet;

public class Fish extends Pet {

    public Fish(){}
    public Fish(String nickname, int age, HashSet<String> habits) {
        super(nickname, age, habits);
        this.setSpecies(PetSpecies.FISH);
    }


    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.getNickname() + ". Я соскучился!");
    }
    @Override
    public String toString() {
        return this.getSpecies() +
                "{nickname=" + this.getNickname() +
                ", age=" + this.getAge() +
                ", habits=" + this.getHabits().toString() + "}";
    }
}
