package pet;

import java.util.HashSet;

public class RoboCat extends Pet{

    public RoboCat(){}
    public RoboCat(String nickname, int age, HashSet<String> habits, PetSpecies species) {
        super(nickname, age, habits);
        this.setSpecies(PetSpecies.ROBOCAT);

    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.getNickname() + ". Я соскучился!");
    }

    @Override
    public String toString() {
        return this.getSpecies() +
                "{nickname=" + this.getNickname() +
                ", age=" + this.getAge() +
                ", habits=" + this.getHabits().toString() + "}";
    }
}
