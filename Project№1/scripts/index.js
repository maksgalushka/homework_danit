"use strict"

// ! OURSERVISCES

const ourServicesMenu = document.querySelectorAll(".our-services-menu-point");
const ourServicesTabs = document.querySelectorAll(".our-services-point-content");

let currentCategory = ourServicesMenu[0].innerText;

const changeCurrentText = () => {
   document.querySelector(".our-services-menu-point-current")?.classList.remove("our-services-menu-point-current");
   document.querySelector(".our-services-point-content-current")?.classList.remove("our-services-point-content-current");
   for (let i = 0; i < ourServicesTabs.length; i++) {
      if (currentCategory === ourServicesTabs[i].dataset.category) {
         ourServicesTabs[i].classList.add("our-services-point-content-current");
         ourServicesMenu[i].classList.add("our-services-menu-point-current");
         console.log(ourServicesTabs[i].classList);
      }
   }
}

ourServicesMenu.forEach(btn => {
   btn.addEventListener('click', (e) => {
      console.log(btn.innerText);
      currentCategory = btn.innerText;
      changeCurrentText()
   })
})

//! OUR WORK TABS


const imgs = document.querySelectorAll(".our-work-item-img");
const ourWorkGaleryDiv = document.querySelector(".our-work-galery");
const loadMoreBtn = document.querySelector(".our-work-btn");
const ourWorkMenuPoints = document.querySelectorAll(".our-work-menu>li")

let currentOurWorkCategory = ourWorkMenuPoints[0].dataset.category;

const changeCurrentOurWorkGalery = () => {



   ourWorkGaleryDiv.innerHTML = ""
   for (let i = 1; i < 13; i++) {
      const wrapper = document.createElement("div");
      wrapper.classList.add("our-work-item-wrapper");
      wrapper.innerHTML = `<img src="./images/OurWorkGalery/${currentOurWorkCategory}/item (${i}).jpg" alt="item1" class="our-work-item-img">
   <div class="our-work-item-hover">
      <div class="our-work-item-hover-btn-wrraper">
         <svg class="our-work-item-hover-btn__link" width="15" height="15" viewBox="0 0 15 15" fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path class="asd" fill-rule="evenodd" clip-rule="evenodd"
               d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.44414 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z"
               fill="#1FDAB5" />
         </svg>
         <div class="our-work-item-hover-btn__play">
            <div class="our-work-item-hover-btn__play-square"></div>
         </div>
      </div>
      <h5>creative design</h5>
      <p>Web Design</p>
   </div>`;
      ourWorkGaleryDiv.append(wrapper);
   }
   if (currentOurWorkCategory === "All") {
      loadMoreBtn.style.display = "inline-block"
   } else {
      loadMoreBtn.style.display = "none"
   }
}

ourWorkMenuPoints.forEach(btn => {
   btn.addEventListener('click', (e) => {
      document.querySelector(".our-work-menu-active__item").classList.remove("our-work-menu-active__item")

      e.target.classList.add("our-work-menu-active__item")
      currentOurWorkCategory = btn.dataset.category
      changeCurrentOurWorkGalery()
   })
})



//! OUR WORK LOAD MORE



let photoNumber = 13

const addMorePic = (e) => {
   for (let i = 0; i < 12; i++) {
      const wrapper = document.createElement("div")
      wrapper.classList.add("our-work-item-wrapper")
      wrapper.innerHTML = `<img src="./images/Our Amazing Work/item (${photoNumber}).jpg" alt="item1" class="our-work-item-img">
   <div class="our-work-item-hover">
      <div class="our-work-item-hover-btn-wrraper">
         <svg class="our-work-item-hover-btn__link" width="15" height="15" viewBox="0 0 15 15" fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path class="asd" fill-rule="evenodd" clip-rule="evenodd"
               d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.44414 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z"
               fill="#1FDAB5" />
         </svg>
         <div class="our-work-item-hover-btn__play">
            <div class="our-work-item-hover-btn__play-square"></div>
         </div>
      </div>
      <h5>creative design</h5>
      <p>Web Design</p>
   </div>`
      ourWorkGaleryDiv.append(wrapper);
      photoNumber++;
   }
   loadMoreBtn.style.display = "none"
}

loadMoreBtn.addEventListener("click", addMorePic)

// ! SLIDER


const peopleList = [
   {
      discription: "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
      name: "HASAN ALI",
      job: "UX Designer",
      url: "./images/People-say/item\ \(1\).jpg",
   },
   {
      discription: "Amet consectetur, adipisicing elit. Vero, quam corrupti exercitationem perspiciatis commodi enim iusto in natus consectetur facilis reprehenderit veritatis mollitia sunt eos, laboriosam accusantium fuga obcaecati odio nemo deleniti? Quis, ipsa necessitatibus laudantium ducimus cumque magni earum.",
      name: "ARTEM NOVIKOV",
      job: "Web designer",
      url: "./images/People-say/item\ \(2\).jpg",
   },
   {
      discription: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vero, quam corrupti exercitationem perspiciatis commodi enim iusto in natus consectetur facilis reprehenderit veritatis mollitia sunt eos, laboriosam accusantium fuga obcaecati odio nemo deleniti? Quis, ipsa necessitatibus laudantium ducimus cumque magni earum.",
      name: "ALINA GURLAK",
      job: "Helper",
      url: "./images/People-say/item\ \(3\).jpg",
   },
   {
      discription: "consectetur adipisicing elit. Adipisci aliquam porro voluptatibus ut, eius debitis corrupti officiis optio nisi exercitationem ad libero accusamus hic ullam sit neque inventore. Est atque deleniti quo nostrum vero ab repellat quam dolore tempora assumenda ut laudantium, repudiandae accusantium harum.",
      name: "DMITRIY GOREN",
      job: "Chief director",
      url: "./images/People-say/item\ \(4\).jpg",
   },
]

let currentPhotoNumber = 1;

const prevBtn = document.querySelector(".prev-btn");
const nextBtn = document.querySelector(".next-btn");
const currentPhoto = document.querySelector(".people__say-current-photo");
const currentDiscription = document.querySelector(".people__say-discription");
const currentName = document.querySelector(".people__say-name");
const currentJob = document.querySelector(".people__say-job");
const peoplePhotoList = document.querySelectorAll(".people__say-item");

const changeCurrentGoal = () => {
   currentPhoto.style.backgroundImage = `url("${peopleList[currentPhotoNumber - 1].url}")`
   currentDiscription.innerText = peopleList[currentPhotoNumber - 1].discription;
   currentName.innerText = peopleList[currentPhotoNumber - 1].name;
   currentJob.innerText = peopleList[currentPhotoNumber - 1].job;
   document.querySelector(".active-slider-photo").classList.remove("active-slider-photo")
   peoplePhotoList[currentPhotoNumber - 1].classList.add("active-slider-photo")
}


peoplePhotoList.forEach((item, index) => {
   item.addEventListener("click", (e) => {
      currentPhotoNumber = index + 1;
      changeCurrentGoal()
   })
})

prevBtn.addEventListener("click", (e) => {
   if (currentPhotoNumber > 1) {
      currentPhotoNumber--;
      changeCurrentGoal()
   }
})

nextBtn.addEventListener("click", (e) => {
   if (currentPhotoNumber < peopleList.length) {
      currentPhotoNumber++;


      changeCurrentGoal()
   }
})





