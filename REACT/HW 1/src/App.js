import React from 'react';
import styles from './App.module.scss';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';

class App extends React.Component {

  state = {
    isOpenFirstModal: false,
    isOpenSecondModal: false,
  }

  render() {

    const { isOpenFirstModal, isOpenSecondModal } = this.state;

    return (
      <div className={styles.main}>


        <Button backgroundColor="red" handleClick={() => {
          this.setState({ isOpenFirstModal: true });
        }}>Open first modal</Button>
        <Button backgroundColor="green" handleClick={() => {
          this.setState({ isOpenSecondModal: true });
        }}>Open second modal</Button>

        {
          isOpenFirstModal && <Modal closeButtton={true} header="Modal1" text="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi eaque sit, inventore alias error sunt aut doloribus dicta perferendis deleniti exercitationem numquam fuga, totam velit perspiciatis accusantium aperiam dolores corporis sequi tempora ratione? Adipisci vero neque quasi modi impedit illum?"
            closeModal={() => {
              this.setState({ isOpenFirstModal: false });
            }}
            actions={(
              <>
                <button>Accept</button>
                <button>Cancel</button>
              </>
            )} />
        }

        {
          isOpenSecondModal && <Modal closeButtton={false} header="Modal2" text="Lorem ipsum dolor sit amet, consectetur adipisicing elit. " actions={(
            <button>Accept</button>
          )} closeModal={() => {
            this.setState({ isOpenSecondModal: false });
          }} />
        }

      </div>
    );
  }


}

export default App;
