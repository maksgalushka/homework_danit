import React, { Children } from "react";
import styles from './Button.module.scss'

class Button extends React.Component {

   render() {

      const { children, handleClick, backgroundColor } = this.props;
      return <button style={{ backgroundColor: backgroundColor }} className={styles.btn} onClick={handleClick} type="button" > {children}</button>
   }
}

export default Button;