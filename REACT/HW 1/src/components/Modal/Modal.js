import React from "react";
import styles from './Modal.module.scss';
import CloseBtn from "./closeBtn/CloseBtn";

class Modal extends React.Component {
   render() {
      const { header, closeButtton, text, actions, closeModal } = this.props;
      return (
         <div onClick={closeModal} className={styles.modalWrraper}>
            <div onClick={e => e.stopPropagation()} className={styles.modal}>
               <div className={styles.modalHeaderWrraper}>
                  <h2>{header}</h2>
                  {closeButtton && <CloseBtn handleClick={closeModal} />}
               </div>
               <p>{text}</p>
               {actions}
            </div>
         </div>)
   }
}

export default Modal;