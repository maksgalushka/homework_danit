import React from "react";
import styles from "./CloseBtn.module.scss"

class CloseBtn extends React.Component {
   render() {
      const { handleClick } = this.props
      return (
         <div onClick={handleClick} className={styles.closeBtn} />
      )
   }
}

export default CloseBtn;