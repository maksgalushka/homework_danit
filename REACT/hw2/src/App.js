import React from 'react'
import styles from './App.module.scss';
import Header from './components/Header/Header';
import CardContainer from "./components/CardContainer/CardContainer";
import Favorite from "./components/Favorite/Favorite";
import CartContainer from "./components/CartContainer/CartContainer";
import Modal from "./components/Modal/Modal";

class App extends React.Component {

  state = {
    favoriteIsOpen: false,
    cartIsOpen: false,
    cards: [],
    carts: [],
    favorite: [],
    isOpenModal: false,
    modalProps: {},
    totalAmount: 0,
    totalFavorite: 0,
  }

  addToCard = (card) => {
    this.setState((current) => {
      const carts = [...current.carts];
      const index = carts.findIndex(el => el.card.name === card.name);
      if (index === -1) {
        carts.push({ card, amount: 1 });
      } else {
        carts[index].amount += 1;
      }

      current.totalAmount += 1

      localStorage.setItem("totalAmount", JSON.stringify(current.totalAmount))
      localStorage.setItem("carts", JSON.stringify(carts))

      return { carts };
    })
  }

  increaseCartItem = (name) => {
    this.setState((current) => {
      const carts = [...current.carts];
      const index = carts.findIndex(el => el.card.name === name);
      if (index !== -1) {
        carts[index].amount += 1
      }

      current.totalAmount += 1

      localStorage.setItem("totalAmount", JSON.stringify(current.totalAmount))
      localStorage.setItem("carts", JSON.stringify(carts))
      return { carts }
    })
  }

  decreaseCartItem = (name) => {
    this.setState((current) => {
      const carts = [...current.carts];
      const index = carts.findIndex(el => el.card.name === name);

      if (index !== -1) {
        carts[index].amount -= 1
        if (carts[index].amount === 0) {
          carts.splice(index, 1)
        }
      }
      current.totalAmount -= 1

      localStorage.setItem("totalAmount", JSON.stringify(current.totalAmount))
      localStorage.setItem("carts", JSON.stringify(carts))
      return { carts }
    })
  }

  deleteCartItem = (name, amount) => {
    this.setState((current) => {
      const carts = [...current.carts];
      const index = carts.findIndex(el => el.card.name === name);
      if (index !== -1) {
        carts.splice(index, 1)
      }
      current.totalAmount = current.totalAmount - amount;
      localStorage.setItem("totalAmount", JSON.stringify(current.totalAmount))
      localStorage.setItem("carts", JSON.stringify(carts))
      return { carts }
    })
  }

  setModalProps = (value) => {
    this.setState({ modalProps: value })
  }

  togleToFavorite = (card) => {
    this.setState((current) => {
      const favorite = [...current.favorite,];
      const index = favorite.findIndex(el => el.name === card.name);
      if (index === -1) {
        favorite.push({ ...card });
      } else {
        favorite.splice(index, 1);
      }

      current.totalFavorite = favorite.length;

      localStorage.setItem("totalFavorite", JSON.stringify(current.totalFavorite))
      localStorage.setItem("favorite", JSON.stringify(favorite))
      return { favorite }
    })
  }


  async componentDidMount() {
    const data = await fetch("./data.json").then(res => res.json())
    const favorite = localStorage.getItem("favorite");
    const carts = localStorage.getItem("carts");
    const totalAmount = localStorage.getItem("totalAmount");
    const totalFavorite = localStorage.getItem("totalFavorite");

    if (totalFavorite) {
      this.setState({ totalFavorite: JSON.parse(totalFavorite) })
    }

    if (favorite) {
      this.setState({ favorite: JSON.parse(favorite) })
    }
    this.setState({ cards: data });

    if (carts) {
      this.setState({ carts: JSON.parse(carts) })
    }
    this.setState({ cards: data });

    this.setState({ totalAmount: JSON.parse(totalAmount) })
  }

  render() {
    const { favoriteIsOpen, cartIsOpen, cards, carts, favorite, modalProps, isOpenModal, totalAmount, totalFavorite } = this.state
    return (
      <div className={styles.App} >
        <Header totalFavorite={totalFavorite} totalAmount={totalAmount} favoriteBtn={() => {
          this.setState({ favoriteIsOpen: !favoriteIsOpen, cartIsOpen: false })
        }}
          cartBtn={() => this.setState({ cartIsOpen: !cartIsOpen, favoriteIsOpen: false })} />
        <main>
          <CardContainer
            setModalProps={this.setModalProps}
            openModal={() => this.setState({ isOpenModal: true })}
            addToCard={this.addToCard}
            togleToFavorite={this.togleToFavorite}
            cards={cards}
            favorite={favorite} />

        </main>
        {favoriteIsOpen && <Favorite
          togleToFavorite={this.togleToFavorite}
          favorite={favorite}
          closeFavorite={() => this.setState({ favoriteIsOpen: false })} />}

        {cartIsOpen && <CartContainer
          setAmount={this.setAmount}
          deleteCartItem={this.deleteCartItem}
          decreaseCartItem={this.decreaseCartItem}
          increaseCartItem={this.increaseCartItem}
          carts={carts}
          closeCart={() => this.setState({ cartIsOpen: false })} />}
        <Modal
          isOpenModal={isOpenModal}
          closeModal={() => this.setState({ isOpenModal: false })}
          modalYesAction={this.addToCard}
          modalProps={modalProps} />
      </div >
    )
  }
}

export default App;
