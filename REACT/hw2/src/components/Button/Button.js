import { PureComponent } from 'react';
import styles from './Button.module.scss';
import PropTypes from 'prop-types';


class Button extends PureComponent {
    render() {
        const { children, action } = this.props
        return (
            <button onClick={action} className={styles.btn} >{children}</button>
        )
    }

}

Button.propTypes = {
    children: PropTypes.string,
    action: PropTypes.func
};

export default Button; 