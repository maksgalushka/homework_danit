import { PureComponent } from 'react';
import styles from "./CartContainer.module.scss";
import CartItem from "../CartItem/CartItem";
import PropTypes from 'prop-types';



class CartContainer extends PureComponent {
    render() {
        const { closeCart, carts, increaseCartItem, decreaseCartItem, deleteCartItem } = this.props;
        return (
            <div className={styles.cartWrraper}>
                <h3 className={styles.title}>Cart</h3>
                <div onClick={closeCart} className={styles.closeBtn} />
                <ul>
                    {
                        carts.map(({ card, amount }) => {
                            return (
                                <li key={card.venderCode}>
                                    <CartItem deleteCartItem={deleteCartItem} decreaseCartItem={decreaseCartItem} increaseCartItem={increaseCartItem} imgUrl={card.imgUrl} amount={amount} name={card.name} />
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
        )
    }
}

CartContainer.CartContainer = {
    closeCart: PropTypes.func,
    carts: PropTypes.array,
    increaseCartItem: PropTypes.func,
    decreaseCartItem: PropTypes.func,
    deleteCartItem: PropTypes.func,
};

CartContainer.defaultProps = {
    carts: [],
};

export default CartContainer; 