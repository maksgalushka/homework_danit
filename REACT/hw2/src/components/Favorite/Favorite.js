import { PureComponent } from 'react';
import styles from "./Favorite.module.scss";
import FavoriteItem from "../FavoriteItem/FavoriteItem";
import PropTypes from 'prop-types';


class Favorite extends PureComponent {
    render() {
        const { closeFavorite, favorite, togleToFavorite } = this.props

        return (
            <div className={styles.favoriteWrraper}>
                <h3 className={styles.title}>Favorite</h3>
                <div onClick={closeFavorite} className={styles.closeBtn} />
                <ul>
                    {favorite.map(({ name, imgUrl, price }) => (
                        <li key={name}>
                            <FavoriteItem togleToFavorite={togleToFavorite} name={name} imgUrl={imgUrl} price={price} />
                        </li>
                    ))}
                </ul>
            </div>
        )
    }
}

Favorite.propTypes = {
    closeFavorite: PropTypes.func,
    favorite: PropTypes.array,
    togleToFavorite: PropTypes.func,
};

export default Favorite; 