import { PureComponent } from 'react';
import styles from "./FavoriteItem.module.scss";
import redHeart from "./redHeart.svg";
import PropTypes from 'prop-types';


class FavoriteItem extends PureComponent {
    render() {
        const { name, imgUrl, price, togleToFavorite } = this.props

        return (
            <div className={styles.favoriteItem}>
                <img className={styles.img} src={imgUrl} alt={name} />
                <h3 className={styles.title}>{name}</h3>
                <span className={styles.price}>{price} грн</span>
                <img onClick={() => { togleToFavorite({ name, price, imgUrl }) }} className={styles.favotriteSvg} src={redHeart} alt="" />
            </div>
        )
    }
}

FavoriteItem.propTypes = {
    name: PropTypes.string,
    imgUrl: PropTypes.string,
    price: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    togleToFavorite: PropTypes.func,
};

export default FavoriteItem; 