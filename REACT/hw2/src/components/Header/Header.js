import { PureComponent } from 'react';
import styles from './Header.module.scss';
import shopingCartImg from "./shopingCart.svg";
import heart from "./heart.svg";
import PropTypes from 'prop-types';

class Header extends PureComponent {


    render() {
        const { favoriteBtn, cartBtn, totalAmount, totalFavorite } = this.props
        return (
            <div className={styles.headerWrapper}>
                <div className={styles.header}>
                    <div className={styles.logo}><span>Mobile</span>Store</div>
                    <div className={styles.navigation}>
                        <img onClick={favoriteBtn} src={heart} alt="" className={styles.favorite} />
                        <div className={styles.amountFavorite}>{totalFavorite}</div>
                        <img onClick={cartBtn} src={shopingCartImg} alt="" className={styles.shopingCart} />
                        <div className={styles.amountCart}>{totalAmount}</div>
                    </div>
                </div>
            </div>
        )
    }
}

Header.propTypes = {
    favoriteBtn: PropTypes.func,
    cartBtn: PropTypes.func,
    totalAmount: PropTypes.number,
    totalFavorite: PropTypes.number,
};

export default Header;