import { useEffect, useState } from 'react'
import styles from './App.module.scss';
import Header from './components/Header/Header';
import CardContainer from "./components/CardContainer/CardContainer";
import Favorite from "./components/Favorite/Favorite";
import CartContainer from "./components/CartContainer/CartContainer";
import Modal from "./components/Modal/Modal";
import { Routes, Route } from "react-router-dom"

const App = () => {
  const [favoriteIsOpen, setFavoriteIsOpen] = useState(false);
  const [cartIsOpen, setCartIsOpen] = useState(false);
  const [cards, setCards] = useState([]);
  const [carts, setCarts] = useState([]);
  const [favorite, setFavorite] = useState([]);
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [modalProps, setModalProps] = useState({});
  const [totalAmount, setTotalAmount] = useState(0);
  const [totalFavorite, setTotalFavorite] = useState(0);

  const addToCard = (card) => {
    setCarts((current) => {
      const carts = [...current];
      const index = carts.findIndex(el => el.card.name === card.name);
      if (index === -1) {
        carts.push({ card, amount: 1 });
      } else {
        carts[index].amount += 1;
      }
      localStorage.setItem("carts", JSON.stringify(carts))



      setTotalAmount(totalAmount => {
        localStorage.setItem("totalAmount", JSON.stringify(totalAmount + 1))
        return totalAmount + 1
      })

      return carts;
    })
  }

  const increaseCartItem = (name) => {
    setCarts((current) => {
      const carts = [...current];
      const index = carts.findIndex(el => el.card.name === name);
      if (index !== -1) {
        carts[index].amount += 1
      }
      localStorage.setItem("carts", JSON.stringify(carts))

      setTotalAmount(totalAmount => {
        localStorage.setItem("totalAmount", JSON.stringify(totalAmount + 1))
        return totalAmount + 1
      })
      return carts;
    })
  }

  const decreaseCartItem = (name) => {
    setCarts((current) => {
      const carts = [...current];
      const index = carts.findIndex(el => el.card.name === name);

      if (index !== -1) {
        carts[index].amount -= 1
        if (carts[index].amount === 0) {
          carts.splice(index, 1)
        }
      }
      localStorage.setItem("carts", JSON.stringify(carts))

      setTotalAmount(totalAmount => {
        localStorage.setItem("totalAmount", JSON.stringify(totalAmount - 1))
        return totalAmount - 1
      })
      return carts;
    })
  }

  const deleteCartItem = (card) => {
    setCarts((current) => {
      const carts = [...current];
      const index = carts.findIndex(el => el.card.name === card.name);
      if (index !== -1) {
        carts.splice(index, 1)
      }
      localStorage.setItem("carts", JSON.stringify(carts));

      setTotalAmount(totalAmount => {
        localStorage.setItem("totalAmount", JSON.stringify(totalAmount - card.amount))
        return totalAmount - card.amount
      })
      return carts;
    })
  }

  const togleToFavorite = (card) => {
    setFavorite((current) => {
      const favorite = [...current];
      const index = favorite.findIndex(el => el.name === card.name);
      if (index === -1) {
        favorite.push({ ...card });
      } else {
        favorite.splice(index, 1);
      }
      localStorage.setItem("favorite", JSON.stringify(favorite))


      setTotalFavorite(favorite.length)
      localStorage.setItem("totalFavorite", JSON.stringify(favorite.length))

      return favorite;
    })
  }

  useEffect(() => {
    const getData = async () => {
      const data = await fetch("./data.json").then(res => res.json())
      const favorite = localStorage.getItem("favorite");
      const carts = localStorage.getItem("carts");
      const totalAmount = localStorage.getItem("totalAmount");
      const totalFavorite = localStorage.getItem("totalFavorite");
      if (totalFavorite) {
        setTotalFavorite(JSON.parse(totalFavorite))
      }

      if (favorite) {
        setFavorite(JSON.parse(favorite))
      }
      setCards(data);

      if (carts) {
        setCarts(JSON.parse(carts))
      }
      setCards(data);


      setTotalAmount(JSON.parse(totalAmount))
    }
    getData()
  }, [])

  return (

    <div className={styles.App} >
      <Header totalFavorite={totalFavorite} totalAmount={totalAmount} favoriteBtn={() => {
        setFavoriteIsOpen(!favoriteIsOpen);
        setCartIsOpen(false);
      }}
        cartBtn={() => {
          setCartIsOpen(!cartIsOpen);
          setFavoriteIsOpen(false);
        }} />
      <Routes>
        <Route path="/" element={
          <CardContainer
            setModalProps={setModalProps}
            openModal={() => setIsOpenModal(true)}
            addToCard={addToCard}
            togleToFavorite={togleToFavorite}
            cards={cards}
            favorite={favorite} />
        } />
        <Route path="/favorite" element={
          <Favorite
            togleToFavorite={togleToFavorite}
            favorite={favorite}
            closeFavorite={() => setFavoriteIsOpen(false)} />} />
        <Route path="/cart" element={
          <CartContainer
            setModalProps={setModalProps}
            openModal={() => setIsOpenModal(true)}
            deleteCartItem={deleteCartItem}
            decreaseCartItem={decreaseCartItem}
            increaseCartItem={increaseCartItem}
            carts={carts}
            closeCart={() => setCartIsOpen(false)} />} />
      </Routes>
      <Modal
        isOpenModal={isOpenModal}
        closeModal={() => setIsOpenModal(false)}
        modalProps={modalProps} />
    </div >
  )
}

export default App;

