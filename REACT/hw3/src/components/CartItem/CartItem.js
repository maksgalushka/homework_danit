import styles from "./CartItem.module.scss";
import PropTypes from 'prop-types';

const CartItem = ({ imgUrl, amount, name, increaseCartItem, decreaseCartItem, deleteCartItem, openModal, setModalProps }) => (
    <div className={styles.cartItem}>
        <img className={styles.image} src={imgUrl} alt={name} />
        <div className={styles.btnWrraper}>
            <div className={styles.amountWrraper}>
                <button onClick={() => {
                    increaseCartItem(name)
                }} className={styles.increaseBtn}>+</button>
                <span className={styles.counter}>{amount}</span>
                <button onClick={() => {
                    decreaseCartItem(name)
                }} className={styles.decreaseBtn}>-</button>
            </div>
            <button onClick={() => {
                openModal()
                setModalProps({ name, amount, action: deleteCartItem, do: "delete" })
                // deleteCartItem(name, amount)
            }} className={styles.deleteBtn}>delete</button>
        </div>
    </div>
)

CartItem.propTypes = {
    imgUrl: PropTypes.string,
    amount: PropTypes.number,
    name: PropTypes.string,
    increaseCartItem: PropTypes.func,
    decreaseCartItem: PropTypes.func,
    deleteCartItem: PropTypes.func,
};



export default CartItem; 