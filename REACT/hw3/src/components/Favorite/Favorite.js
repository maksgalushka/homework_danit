import styles from "./Favorite.module.scss";
import FavoriteItem from "../FavoriteItem/FavoriteItem";
import PropTypes from 'prop-types';
import { Link } from "react-router-dom"

const Favorite = ({ closeFavorite, favorite, togleToFavorite }) => (
    <div className={styles.favoriteWrraper}>
        <h3 className={styles.title}>Your FAVORITE</h3>
        <Link className={styles.returnBtn} to="/">BACK</Link>
        <ul>
            {favorite.map(({ name, imgUrl, price }) => (
                <li key={name}>
                    <FavoriteItem togleToFavorite={togleToFavorite} name={name} imgUrl={imgUrl} price={price} />
                </li>
            ))}
        </ul>
    </div>
)

Favorite.propTypes = {
    closeFavorite: PropTypes.func,
    favorite: PropTypes.array,
    togleToFavorite: PropTypes.func,
};

export default Favorite; 