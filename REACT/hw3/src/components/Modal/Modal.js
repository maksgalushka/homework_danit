import styles from './Modal.module.scss';
import PropTypes from 'prop-types';

const Modal = ({ isOpenModal, closeModal, modalYesAction, modalProps }) => {
    if (!isOpenModal) {
        return null;
    }

    return (
        <div onClick={closeModal} className={styles.modalWrraper}>
            <div onClick={e => e.stopPropagation()} className={styles.modal}>
                <div className={styles.modalHeaderWrraper}>
                    <h2>Do you realy want {modalProps.do} {modalProps.name}?</h2>
                    <div onClick={closeModal} className={styles.closeBtn} />
                </div>
                <p></p>
                <button onClick={() => {
                    console.log(modalProps);
                    modalProps.action(modalProps);
                    closeModal()
                }}>Yes</button>
                <button onClick={closeModal}>No</button>
            </div>
        </div >
    )
}


Modal.propTypes = {
    isOpenModal: PropTypes.bool,
    closeModal: PropTypes.func,
    modalYesAction: PropTypes.func,
};

export default Modal; 