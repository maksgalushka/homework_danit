import CardItem from "../CardItem/CardItem"
import styles from "./CardContainer.module.scss";
import PropTypes from 'prop-types';
import { useSelector } from "react-redux";

const CardContainer = ({ togleToFavorite, favorite, addToCard, setModalProps }) => {

    const cards = useSelector(store => store.cards.getData);

    return (
        <div>
            <ul className={styles.list}>
                {cards.map(({ name, price, imgUrl, venderCode, color }) => (
                    <li key={venderCode}>
                        <CardItem
                            setModalProps={setModalProps}
                            addToCard={addToCard}
                            togleToFavorite={togleToFavorite}
                            name={name}
                            price={price}
                            imgUrl={imgUrl}
                            venderCode={venderCode}
                            color={color}
                            favorite={favorite} />
                    </li>
                ))}
            </ul>
        </div >
    )
}


CardContainer.propTypes = {
    togleToFavorite: PropTypes.func,
    cards: PropTypes.array,
    favorite: PropTypes.array,
    addToCard: PropTypes.func,
    setModalProps: PropTypes.func,
    openModal: PropTypes.func,
};

CardContainer.defaultProps = {
    cards: [],
    favorite: [],
};

export default CardContainer; 