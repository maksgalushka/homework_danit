import styles from './CardItem.module.scss';
import heart from "./heart.svg";
import redHeart from "./redHeart.svg";
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { setModalOpenAC } from '../../store/modal/actionsCreators';

const CardItem = ({ name, price, imgUrl, color, togleToFavorite, favorite, venderCode, setModalProps, addToCard }) => {

    const dispatch = useDispatch()

    return (
        <div className={styles.card} >
            <img onClick={() => {
                togleToFavorite({ name, price, imgUrl });
            }} className={styles.likeBtn} src={(() => {
                const index = favorite.findIndex(el => el.name === name);
                if (index === -1) {
                    return heart;
                } else {
                    return redHeart;
                }
            })()} alt="like" />
            <img className={styles.cardImg} src={imgUrl} alt={name} />
            <p className={styles.cardName}>{name}</p>
            <span className={styles.price}>{price} грн</span>
            <div className={styles.color} style={{ backgroundColor: color }} ></div>
            <button className={styles.addToCartBtn} onClick={() => {

                dispatch(setModalOpenAC(true))
                setModalProps({ name, imgUrl, venderCode, action: addToCard, do: "add" });
            }}>add to cart</button>
        </div >
    )
}


CardItem.propTypes = {
    name: PropTypes.string,
    price: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    imgUrl: PropTypes.string,
    color: PropTypes.string,
    togleToFavorite: PropTypes.func,
    favorite: PropTypes.array,
    venderCode: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    setModalProps: PropTypes.func,
    openModal: PropTypes.func,
};

export default CardItem; 