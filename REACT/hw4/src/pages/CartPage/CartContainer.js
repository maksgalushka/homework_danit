import styles from "./CartContainer.module.scss";
import CartItem from "../../components/CartItem/CartItem";
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";

const CartContainer = ({ carts, increaseCartItem, decreaseCartItem, deleteCartItem, setAmountAtLocalStorage, setModalProps }) => (
    <div className={styles.cartWrraper}>
        <h3 className={styles.title}>Your CART</h3>
        <Link className={styles.returnBtn} to="/">BACK</Link>
        <ul>
            {
                carts.map(({ card, amount }) => {
                    return (
                        <li key={card.venderCode}>
                            <CartItem setModalProps={setModalProps} setAmountAtLocalStorage={setAmountAtLocalStorage} deleteCartItem={deleteCartItem} decreaseCartItem={decreaseCartItem} increaseCartItem={increaseCartItem} imgUrl={card.imgUrl} amount={amount} name={card.name} />
                        </li>
                    )
                })
            }
        </ul>
    </div>
)


CartContainer.CartContainer = {
    closeCart: PropTypes.func,
    carts: PropTypes.array,
    increaseCartItem: PropTypes.func,
    decreaseCartItem: PropTypes.func,
    deleteCartItem: PropTypes.func,
};

CartContainer.defaultProps = {
    carts: [],
};

export default CartContainer; 