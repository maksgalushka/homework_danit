import { combineReducers } from "redux";
import cardsReducer from "./cards/reducer";
import modalReducer from "./modal/reducer";

const appReducer = combineReducers({
    cards: cardsReducer,
    modal: modalReducer,
})

export default appReducer;