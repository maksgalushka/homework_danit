import { useEffect, useState } from 'react'
import styles from './App.module.scss';
import Header from './components/Header/Header';
import CardContainer from "./components/CardContainer/CardContainer";
import Favorite from "./pages/FavoritePage/Favorite";
import CartContainer from "./pages/CartPage/CartContainer";
import Modal from "./components/Modal/Modal";
import { Routes, Route } from "react-router-dom";
import { useDispatch } from "react-redux";
import { getDataAC } from './store/cards/actionsCreators';


const App = () => {
  const [favoriteIsOpen, setFavoriteIsOpen] = useState(false);
  const [cartIsOpen, setCartIsOpen] = useState(false);
  const [carts, setCarts] = useState([]);
  const [favorite, setFavorite] = useState([]);
  const [modalProps, setModalProps] = useState({});
  const [totalAmount, setTotalAmount] = useState();
  const [totalFavorite, setTotalFavorite] = useState();

  const dispatch = useDispatch();

  const addToCard = (card) => {
    setCarts((current) => {
      const carts = [...current];
      console.log(current);
      const index = carts.findIndex(el => el.card.name === card.name);
      if (index === -1) {
        carts.push({ card, amount: 1 });
      } else {
        carts[index].amount += 1;
      }
      localStorage.setItem("carts", JSON.stringify(carts))



      setTotalAmount(totalAmount => {
        localStorage.setItem("totalAmount", JSON.stringify(totalAmount + 1))
        return totalAmount + 1
      })

      return carts;
    })
  }

  const increaseCartItem = (name) => {
    setCarts((current) => {
      const carts = [...current];
      const index = carts.findIndex(el => el.card.name === name);
      if (index !== -1) {
        carts[index].amount += 1
      }
      localStorage.setItem("carts", JSON.stringify(carts))

      setTotalAmount(totalAmount => {
        localStorage.setItem("totalAmount", JSON.stringify(totalAmount + 1))
        return totalAmount + 1
      })
      return carts;
    })
  }

  const decreaseCartItem = (name) => {
    setCarts((current) => {
      const carts = [...current];
      const index = carts.findIndex(el => el.card.name === name);

      if (index !== -1) {
        carts[index].amount -= 1
        if (carts[index].amount === 0) {
          carts.splice(index, 1)
        }
      }
      localStorage.setItem("carts", JSON.stringify(carts))

      setTotalAmount(totalAmount => {
        localStorage.setItem("totalAmount", JSON.stringify(totalAmount - 1))
        return totalAmount - 1
      })
      return carts;
    })
  }

  const deleteCartItem = (card) => {
    setCarts((current) => {
      const carts = [...current];
      const index = carts.findIndex(el => el.card.name === card.name);
      if (index !== -1) {
        carts.splice(index, 1)
      }
      localStorage.setItem("carts", JSON.stringify(carts));

      setTotalAmount(totalAmount => {
        localStorage.setItem("totalAmount", JSON.stringify(totalAmount - card.amount))
        return totalAmount - card.amount
      })
      return carts;
    })
  }

  const resetCart = () => {
    setCarts([])
    setTotalAmount(0)
    localStorage.removeItem("carts")
    setTotalAmount(0)
    localStorage.setItem("totalAmount", JSON.stringify(0))
  }
  const togleToFavorite = (card) => {
    setFavorite((current) => {
      console.log(current);
      const favorite = [...current];
      const index = favorite.findIndex(el => el.name === card.name);
      if (index === -1) {
        favorite.push({ ...card });
      } else {
        favorite.splice(index, 1);
      }
      localStorage.setItem("favorite", JSON.stringify(favorite))


      setTotalFavorite(favorite.length)
      localStorage.setItem("totalFavorite", JSON.stringify(favorite.length))

      return favorite;
    })
  }

  useEffect(() => {

    dispatch(getDataAC())
    const getData = async () => {
      const favorite = localStorage.getItem("favorite");
      const carts = localStorage.getItem("carts");
      const totalAmount = localStorage.getItem("totalAmount");
      const totalFavorite = localStorage.getItem("totalFavorite");
      if (totalFavorite) {
        setTotalFavorite(JSON.parse(totalFavorite))
      }

      if (favorite) {
        setFavorite(JSON.parse(favorite))
      }

      if (carts) {
        setCarts(JSON.parse(carts))
      }


      setTotalAmount(JSON.parse(totalAmount))
    }
    getData()

  }, []);


  return (

    <div className={styles.App} >
      <Header totalFavorite={totalFavorite} totalAmount={totalAmount} favoriteBtn={() => {
        setFavoriteIsOpen(!favoriteIsOpen);
        setCartIsOpen(false);
      }}
        cartBtn={() => {
          setCartIsOpen(!cartIsOpen);
          setFavoriteIsOpen(false);
        }} />
      <Routes>
        <Route path="/" element={
          <CardContainer
            setModalProps={setModalProps}
            addToCard={addToCard}
            togleToFavorite={togleToFavorite}
            favorite={favorite} />
        } />
        <Route path="/favorite" element={
          <Favorite
            setModalProps={setModalProps}
            addToCard={addToCard}
            togleToFavorite={togleToFavorite}
            favorite={favorite}
            closeFavorite={() => setFavoriteIsOpen(false)} />} />
        <Route path="/cart" element={
          <CartContainer
            resetCart={resetCart}
            setModalProps={setModalProps}
            deleteCartItem={deleteCartItem}
            decreaseCartItem={decreaseCartItem}
            increaseCartItem={increaseCartItem}
            carts={carts}
            closeCart={() => setCartIsOpen(false)} />} />
      </Routes>
      <Modal
        modalProps={modalProps} />
    </div >
  )
}

export default App;

