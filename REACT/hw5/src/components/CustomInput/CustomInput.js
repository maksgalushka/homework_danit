import styles from "./CustomInput.module.scss";
import PropTypes from "prop-types"
import { useField } from "formik";


const CustomInput = (props) => {
    const { type, title } = props

    const [field, meta] = useField(props);


    return (
        <div className={styles.inputWrraper}>
            <div className={styles.titleWrraper}>
                <span className={styles.inputTitle}>{title}</span>
                {meta.touched && <span className={styles.error}> {meta.error}</span>}
            </div>
            <input
                className={styles.formInput}
                type={type}
                {...field}
            />
        </div>
    )
}

CustomInput.propTypes = {
    type: PropTypes.string,
    title: PropTypes.string,
    name: PropTypes.string,
}

CustomInput.deafultProps = {
    type: "text",
}


export default CustomInput