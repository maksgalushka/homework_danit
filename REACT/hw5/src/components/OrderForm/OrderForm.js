import styles from "./OrderForm.module.scss";
import { Formik, Form, Field } from "formik";
import * as yup from "yup";
import PropTypes from "prop-types";
import CustomInput from "../CustomInput/CustomInput";

const OrderForm = ({ carts, resetCart }) => {

    const initialValues = {
        firstname: "",
        lastname: "",
        age: "",
        address: "",
        mobile: "",
        comment: "",
    }

    const validationSchema = yup.object().shape({
        firstname: yup.string()
            .min(2, 'Min 1 symbol')
            .max(20, 'Max 20 symbols')
            .required('firstname field is required'),
        lastname: yup.string()
            .min(2, 'Min 1 symbol')
            .max(20, 'Max 20 symbols')
            .required('lastname field is required'),
        age: yup.number().typeError("Is not a number")
            .required('age field is required')
            .positive('incorrect age')
            .integer('incorrect age'),
        address: yup.string()
            .min(5, 'Min 5 symbol')
            .max(40, 'Max 40 symbols')
            .required('address field is required'),
        mobile: yup.number().typeError("Is not a number")
            .required('age field is required')

    })

    return (<Formik
        className={styles.formWrraper}
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values, { resetForm }) => {
            console.log(values);
            console.log(carts);
            resetForm();
            resetCart();
        }}
    >
        <Form className={styles.form}>

            <div className={styles.oneLineWrraper}>
                <CustomInput
                    type="text"
                    name="firstname"
                    title="Firstname"
                />
                <CustomInput
                    type="text"
                    name="lastname"
                    title="Lastname"
                />
            </div>

            <div className={styles.oneLineWrraper}>
                <CustomInput
                    type="text"
                    name="age"
                    title="Age"
                />
                <CustomInput
                    type="text"
                    name="address"
                    title="Address"
                />
                <CustomInput
                    type="tel"
                    name="mobile"
                    title="Mobile"
                />
            </div>
            <div className={styles.inputWrraper}>
                <span className={styles.inputTitle}>Your comment</span>
                <Field
                    className={styles.formInput}
                    type="text"
                    name="comment"
                    as="textarea"
                    placeholder="optional"
                />
            </div>


            <button disabled={carts.length === 0} type="submit" className={styles.submitBtn}>
                Chekout
            </button>

        </Form>
    </Formik>)
}

OrderForm.CartContainer = {
    resetCart: PropTypes.func,
    carts: PropTypes.array,
};

OrderForm.defaultProps = {
    carts: [],
};

export default OrderForm; 