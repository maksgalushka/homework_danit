import { SET_MODAL_OPEN } from "./actions";

export const setModalOpenAC = (payload) => ({ type: SET_MODAL_OPEN, payload })