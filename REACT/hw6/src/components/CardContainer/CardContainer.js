import { useContext } from "react";
import CardItem from "../CardItem/CardItem";
import styles from "./CardContainer.module.scss";
import PropTypes from 'prop-types';
import rowsSvg from './rows.svg';
import columnSvg from './grid.svg'
import { useSelector } from "react-redux";
import { Context } from "../../App";

const CardContainer = ({ togleToFavorite, favorite, addToCard, setModalProps }) => {

    const cards = useSelector(store => store.cards.getData);
    const { isColumns, setIsColumns } = useContext(Context);

    return (
        <div>
            <div className={styles.title}>
                <h2>PRODUCT LIST</h2>
                <img
                    className={styles.imgTheme}
                    src={(isColumns ? columnSvg : rowsSvg)}
                    onClick={() => setIsColumns(prev => !prev)}
                    alt="rows" />
            </div>
            <ul className={styles.list}>

                {cards.map(({ name, price, imgUrl, venderCode, color }) => (
                    <li key={venderCode}>
                        <CardItem
                            setModalProps={setModalProps}
                            addToCard={addToCard}
                            togleToFavorite={togleToFavorite}
                            name={name}
                            price={price}
                            imgUrl={imgUrl}
                            venderCode={venderCode}
                            color={color}
                            favorite={favorite} />
                    </li>
                ))}
            </ul>
        </div >
    )
}


CardContainer.propTypes = {
    togleToFavorite: PropTypes.func,
    cards: PropTypes.array,
    favorite: PropTypes.array,
    addToCard: PropTypes.func,
    setModalProps: PropTypes.func,
    openModal: PropTypes.func,
};

CardContainer.defaultProps = {
    cards: [],
    favorite: [],
};

export default CardContainer; 