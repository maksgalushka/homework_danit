import styles from "./FavoriteItem.module.scss";
import redHeart from "./redHeart.svg";
import PropTypes from 'prop-types';
import { useDispatch } from "react-redux";
import { setModalOpenAC } from "../../store/modal/actionsCreators";

const FavoriteItem = ({ name, imgUrl, price, venderCode, addToCard, togleToFavorite, setModalProps, }) => {

    const dispatch = useDispatch()

    return (
        <div className={styles.favoriteItem}>
            <img className={styles.img} src={imgUrl} alt={name} />
            <h3 className={styles.title}>{name}</h3>
            <span className={styles.price}>{price} грн</span>
            <img onClick={() => { togleToFavorite({ name, price, imgUrl }) }} className={styles.favotriteSvg} src={redHeart} alt="" />

            <button className={styles.addToCartBtn} onClick={() => {

                dispatch(setModalOpenAC(true))
                setModalProps({ name, imgUrl, venderCode, action: addToCard, do: "add" });
            }}>add to cart</button>
        </div>
    )
}

FavoriteItem.propTypes = {
    name: PropTypes.string,
    imgUrl: PropTypes.string,
    price: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    togleToFavorite: PropTypes.func,
};

export default FavoriteItem; 