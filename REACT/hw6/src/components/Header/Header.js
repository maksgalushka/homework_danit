import styles from './Header.module.scss';
import shopingCartImg from "./shopingCart.svg";
import heart from "./heart.svg";
import PropTypes from 'prop-types';
import { Link } from "react-router-dom"

const Header = ({ favoriteBtn, cartBtn, totalAmount, totalFavorite }) => (
    <div className={styles.headerWrapper}>
        <div className={styles.header}>
            <Link className={styles.logo} to="/">
                <span>Mobile</span>Store
            </Link>
            <div className={styles.navigation}>
                <Link to="/favorite">
                    <img onClick={favoriteBtn} src={heart} alt="" className={styles.favorite} />
                </Link>
                <div className={styles.amountFavorite}>{totalFavorite}</div>
                <Link to="/cart">
                    <img onClick={cartBtn} src={shopingCartImg} alt="" className={styles.shopingCart} />
                </Link>
                <div className={styles.amountCart}>{totalAmount}</div>
            </div>
        </div>
    </div>
)

Header.propTypes = {
    favoriteBtn: PropTypes.func,
    cartBtn: PropTypes.func,
    totalAmount: PropTypes.number,
    totalFavorite: PropTypes.number,
};

Header.defaultProps = {
    favoriteBtn: () => { },
    cartBtn: () => { },
    totalAmount: 0,
    totalFavorite: 0,
};

export default Header;