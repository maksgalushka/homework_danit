import Header from "./Header";
import { render } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";

const Component = () => (
    <BrowserRouter>
        <Header />
    </BrowserRouter>
)


describe('Header reders', () => {
    test('Should Header render', () => {
        const { asFragment } = render(<Component />);
        expect(asFragment()).toMatchSnapshot();
    })
})