import styles from './Modal.module.scss';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { setModalOpenAC } from '../../store/modal/actionsCreators';

const Modal = ({ modalProps }) => {
    const dispatch = useDispatch()
    const isOpenModal = useSelector(store => store.modal.isOpenModal);


    if (!isOpenModal) {
        return null;
    }

    return (
        <div
            data-testid='Modal-bgd'
            onClick={() => dispatch(setModalOpenAC(false))}
            className={styles.modalWrraper}>
            <div onClick={e => e.stopPropagation()} className={styles.modal}>
                <div className={styles.modalHeaderWrraper}>
                    <h2>Do you realy want {modalProps.do} {modalProps.name}?</h2>
                    <div
                        data-testid='Modal-close-btn'
                        onClick={() => dispatch(setModalOpenAC(false))}
                        className={styles.closeBtn} />
                </div>
                <p></p>
                <button onClick={() => {
                    modalProps.action(modalProps);
                    dispatch(setModalOpenAC(false))
                }}>Yes</button>
                <button onClick={() => dispatch(setModalOpenAC(false))}>No</button>
            </div>
        </div >
    )
}


Modal.propTypes = {
    isOpenModal: PropTypes.bool,
    closeModal: PropTypes.func,
    modalYesAction: PropTypes.func,
    modalProps: PropTypes.object,
};

Modal.defaultProps = {
    modalProps: {
        action: () => { }
    },
};

export default Modal; 