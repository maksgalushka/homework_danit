import Modal from "./Modal";
import { fireEvent, render, screen } from "@testing-library/react";
import { Provider, useDispatch } from "react-redux";
import store from '../../store/index';
import { setModalOpenAC } from "../../store/modal/actionsCreators";


const Component = () => {
    const dispatch = useDispatch();
    dispatch(setModalOpenAC(false));

    return (
        <>
            <button onClick={() => dispatch(setModalOpenAC(true))}>
                OPEN
            </button>
            <Modal />
        </>
    )
}

const MockedProvider = () => (
    <Provider store={store}>
        <Component />
    </Provider>
)


describe('Modal reders', () => {
    test('Should Modal no render without modalOpen props', () => {
        const { asFragment } = render(<MockedProvider />);
        expect(asFragment()).toMatchSnapshot();
    })

    test('Should Modal render with modalOpen props', () => {
        const { asFragment } = render(<MockedProvider />);
        fireEvent.click(screen.getByText('OPEN'));
        expect(asFragment()).toMatchSnapshot();
    });

})

describe('Modal close', () => {
    test('should Modal background close works', () => {
        render(<MockedProvider />);
        fireEvent.click(screen.getByText('OPEN'));
        const bgd = screen.getByTestId("Modal-bgd");
        fireEvent.click(bgd);
        expect(screen.queryByTestId("Modal-bgd")).not.toBeInTheDocument();
    });

    test('should Modal NO-btn  close works', () => {
        render(<MockedProvider />);
        fireEvent.click(screen.getByText('OPEN'));
        const btn = screen.getByText("No");
        fireEvent.click(btn);
        expect(screen.queryByText("No")).not.toBeInTheDocument();
    });

    test('should Modal Close-btn  close works', () => {
        render(<MockedProvider />);
        fireEvent.click(screen.getByText('OPEN'));
        const btn = screen.getByTestId('Modal-close-btn');
        fireEvent.click(btn);
        expect(screen.queryByTestId('Modal-close-btn')).not.toBeInTheDocument();
    });

    test('should Modal YES-btn close works', () => {
        render(<MockedProvider />);
        fireEvent.click(screen.getByText('OPEN'));
        const btn = screen.getByText("Yes");
        fireEvent.click(btn);
        expect(screen.queryByText("Yes")).not.toBeInTheDocument();
    });
})