import styles from "./CartContainer.module.scss";
import CartItem from "../../components/CartItem/CartItem";
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import OrderForm from "../../components/OrderForm/OrderForm";

const CartContainer = ({ carts, increaseCartItem, decreaseCartItem, deleteCartItem, setAmountAtLocalStorage, setModalProps, resetCart }) => {

    return (
        <div className={styles.cartWrraper}>
            <h3 className={styles.title}>Placing an order</h3>
            <hr />
            <Link className={styles.returnBtn} to="/">BACK</Link>
            <OrderForm
                carts={carts}
                resetCart={resetCart}
            ></OrderForm>
            <h3 className={styles.title}>Your CART</h3>
            <hr />
            <ul className={styles.list}>
                {
                    carts.map(({ card, amount }) => {
                        return (
                            <li key={card.venderCode}>
                                <CartItem setModalProps={setModalProps} setAmountAtLocalStorage={setAmountAtLocalStorage} deleteCartItem={deleteCartItem} decreaseCartItem={decreaseCartItem} increaseCartItem={increaseCartItem} imgUrl={card.imgUrl} amount={amount} name={card.name} venderCode={card.venderCode} />
                            </li>
                        )
                    })
                }
            </ul>
            {(carts.length === 0) && <h2 className={styles.emptyTitle}>Your cart is empty!</h2>}
        </div>
    )
}


CartContainer.CartContainer = {
    closeCart: PropTypes.func,
    carts: PropTypes.array,
    increaseCartItem: PropTypes.func,
    decreaseCartItem: PropTypes.func,
    deleteCartItem: PropTypes.func,
};

CartContainer.defaultProps = {
    carts: [],
};

export default CartContainer; 