import styles from "./Favorite.module.scss";
import FavoriteItem from "../../components/FavoriteItem/FavoriteItem";
import PropTypes from 'prop-types';
import { Link } from "react-router-dom"

const Favorite = ({ favorite, togleToFavorite, addToCard, setModalProps }) => (
    <div className={styles.favoriteWrraper}>
        <h3 className={styles.title}>Your FAVORITE</h3>
        <Link className={styles.returnBtn} to="/">BACK</Link>
        <ul className={styles.list}>
            {favorite.map(({ name, imgUrl, price, venderCode }) => (
                <li key={name}>
                    <FavoriteItem setModalProps={setModalProps} addToCard={addToCard} togleToFavorite={togleToFavorite} name={name} imgUrl={imgUrl} price={price} venderCode={venderCode} />
                </li>
            ))}
        </ul>
    </div>
)

Favorite.propTypes = {
    closeFavorite: PropTypes.func,
    favorite: PropTypes.array,
    togleToFavorite: PropTypes.func,
};

export default Favorite; 