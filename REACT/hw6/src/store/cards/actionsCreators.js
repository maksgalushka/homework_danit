import { GET_DATA } from "./actions";

export const getDataAC = () => async (dispatch) => {
    const data = await fetch("./data.json").then(res => res.json())
    dispatch({ type: GET_DATA, payload: data })
};