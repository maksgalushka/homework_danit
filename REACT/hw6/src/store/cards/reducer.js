import { GET_DATA } from "./actions"
const initialValue = {
    getData: []
}

const cardsReducer = (state = initialValue, action) => {
    switch (action.type) {
        case GET_DATA: {
            return { getData: action.payload };
        }
        default: {
            return state;
        }
    }
}

export default cardsReducer;