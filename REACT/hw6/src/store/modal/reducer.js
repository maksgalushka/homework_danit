import { SET_MODAL_OPEN } from "./actions"
const initialValue = {
    isOpenModal: false,
}

const modalReducer = (state = initialValue, action) => {
    switch (action.type) {
        case SET_MODAL_OPEN: {
            return { isOpenModal: action.payload };
        }
        default: {
            return state;
        }
    }
}

export default modalReducer;