//? Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Прототипне наслідування в Javascript - це відтворення об'єкта на основі іншого
//? Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Для того щоб передати властивості батьківського класса 
"use strict"

class Employee {
   constructor(name, age, salary) {
      this._name = name;
      this._age = age;
      this._salary = salary;
   }

   get name() {
      return this._name
   }

   set name(newName) {
      this._name = newName
   }

   get age() {
      return this._age
   }

   set age(newAge) {
      this._age = newAge
   }

   get salary() {
      return this._salary
   }

   set salary(newSalary) {
      this._salary = newSalary
   }
}

class Programmer extends Employee {
   constructor(name, age, salary, lang) {
      super(name, age, salary)
      this.lang = lang;
   }

   get salary() {
      return this._salary * 3
   }
}

const firstProgrammer = new Programmer("Jhon", 20, 250, "JS");
const secondProgrammer = new Programmer("Alex", 32, 400, "C++");
const thirdProgrammer = new Programmer("Bob", 28, 200, "Python");

console.log(firstProgrammer);
console.log(secondProgrammer);
console.log(thirdProgrammer);

