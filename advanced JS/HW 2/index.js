//? Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch
// try... catch используют для того что бы отловить исключение. 
// В качестве примера использования try... catch можно взять отрисовку большого колличества однотипных данных. Так же try... catch исполюзуют для декодирования данных, полученных по сети, от сервера или из другого источника.
"use strict"

const books = [
   {
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70
   },
   {
      author: "Сюзанна Кларк",
      name: "Джонатан Стрейндж і м-р Норрелл",
   },
   {
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
   },
   {
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
   },
   {
      author: "Террі Пратчетт",
      name: "Рухомі картинки",
      price: 40
   },
   {
      author: "Анґус Гайленд",
      name: "Коти в мистецтві",
   }
];

class IncompleteBookError extends Error {
   constructor(missingProp) {
      super();
      this.name = "IncompleteBookError";
      this.message = `Invalid format of book. Your book haven't "${missingProp}" property`;
   }
}


class ListItem {
   constructor(author, name, price) {
      if (!author) {
         throw new IncompleteBookError("author");
      } else if (!name) {
         throw new IncompleteBookError("name");
      } else if (!price) {
         throw new IncompleteBookError("price");
      }
      this.author = author;
      this.name = name;
      this.price = price;
   }

   render(container) {
      container.insertAdjacentHTML('beforeend', `
      <li>
      <h3>Name: ${this.name}</h3>
      <p>Author: ${this.author}</p>
      <p>Price: ${this.price}</p>
      </li>
      `)
   }
}

const renderAndValidateItems = () => {
   const container = document.querySelector("#root");
   const ul = document.createElement("ul")
   container.append(ul);
   books.forEach(el => {
      try {
         new ListItem(el.author, el.name, el.price).render(container);
      } catch (err) {
         if (err.name === "IncompleteBookError") {
            console.error(err);
         } else {
            throw err;
         }
      }

   })
}

renderAndValidateItems()
