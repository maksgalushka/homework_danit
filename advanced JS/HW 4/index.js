// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
// При использовании AJAX нет необходимости обновлять каждый раз всю страницу, так как обновляется только ее конкретная часть
"use strict"
const url = "https://ajax.test-danit.com/api/swapi/films"

const main = document.querySelector(".main")

const getAndProcessingData = (link, operation) => {
   fetch(link)
      .then(response => response.json())
      .then(operation)
}

const createAndRenderFilmElements = (episode) => {
   episode.forEach(({ characters, episodeId, name, openingCrawl }) => {
      const episodeDiv = document.createElement("div")
      episodeDiv.classList.add(`episode`);
      episodeDiv.insertAdjacentHTML("beforeend", `
      <h2>${name.toUpperCase()}</h2>
      <h4>Episode number: ${episodeId}</h4>
      <p>${openingCrawl}</p>
      `)

      const charactersList = document.createElement("div");
      charactersList.classList.add("characters-wrraper");
      const loader = document.createElement("div");
      loader.classList.add("loader");
      charactersList.insertAdjacentElement("beforeend", loader);
      episodeDiv.insertAdjacentElement("beforeend", charactersList);

      characters.forEach(el => {
         getAndProcessingData(el, ({ birthYear, eyeColor, gender, hairColor, height, mass, name, skinColor }) => {
            charactersList.insertAdjacentHTML("beforeend", `
                           <div class="character">
                              <h3 class="character-name">${name}</h3>
                              <p class="character-prop">Birth year: ${birthYear}</p>
                              <p class="character-prop">Eye color: ${eyeColor}</p>
                              <p class="character-prop">Gender: ${gender}</p>
                              <p class="character-prop">Hair color: ${hairColor}</p>
                              <p class="character-prop">Height: ${height}</p>
                              <p class="character-prop">Mass: ${mass}</p>
                              <p class="character-prop">Skin color: ${skinColor}</p>
                           </div>
                        `)
            loader.style.display = "none"
         })
      })
      main.insertAdjacentElement("beforeend", episodeDiv)
   })
   document.querySelector(".loader").style.display = "none"
}

getAndProcessingData(url, createAndRenderFilmElements)