"use strict"

class Card {
   constructor(title, text, userName, email, postId) {
      this.container = document.createElement("div");
      this.deleteButton = document.createElement("button");
      this.title = title;
      this.text = text;
      this.userName = userName;
      this.email = email;
      this.postId = postId;
   }
   createElement() {
      this.container.className = "card";
      this.container.innerHTML = `
      <h3 class="card__user-name">${this.userName}<span class="card__email">${this.email}</span></h3> 
      <h4 class="card__title">${this.title}</h4>
      <p class="card__text">${this.text}</p>
      `
      this.deleteButton.className = "card__delete-btn"
      this.container.append(this.deleteButton);

      this.deleteButton.addEventListener("click", (event) => {
         fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
            method: 'DELETE',
         })
            .then(response => {
               if (response.status === 200) {
                  event.target.parentElement.remove();
               }
            })
      });
   }

   render(selector) {
      this.createElement();
      document.querySelector(selector).append(this.container);
   }
}

const createPosts = () => {
   fetch("https://ajax.test-danit.com/api/json/users")
      .then(response => response.json())
      .then(userData => {
         fetch("https://ajax.test-danit.com/api/json/posts")
            .then(response => response.json())
            .then(postsData => {
               userData.forEach(userEl => {
                  postsData.forEach(postEl => {
                     const post = new Card(postEl.title, postEl.body, userEl.username, userEl.email, postEl.id);
                     post.render(".posts");
                  })
               })
            })
      }
      )
}

createPosts()
