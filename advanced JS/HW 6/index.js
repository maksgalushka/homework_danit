// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
// Асинхронности в JavaScript как таковой не существует, весь JavaScript код синхронен, но за счёт выполнения кода частями создаёться впечетление что код асинхронен.

async function findLocation() {
   let fetchIp = await fetch("https://api.ipify.org/?format=json");
   let responseIp = await fetchIp.json();
   let fetchLoc = await fetch(`http://ip-api.com/json/${responseIp.ip}?fields=continent,country,regionName,city`);
   let responseLoc = await fetchLoc.json();
   let dataLoc = document.querySelector(".data").innerHTML = `
   <p>Continent: ${responseLoc.continent}</p>
   <p>Country: ${responseLoc.country}</p>
   <p>Region name: ${responseLoc.regionName}</p>
   <p>City: ${responseLoc.city}</p>
   `
}

document.querySelector("button").addEventListener("click", findLocation)