import gulp from 'gulp';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import browserSync from 'browser-sync';
import concat from 'gulp-concat';
import terser from 'gulp-terser';
import cleancss from "gulp-clean-css";
import imagemin from 'gulp-imagemin';
import autoprefixer from 'gulp-autoprefixer';
import clean from 'gulp-clean';

const BS = browserSync.create();
const sass = gulpSass(dartSass);

const buildStyles = () => gulp.src("./src/styles/**/*.scss")
   .pipe(sass())
   .pipe(concat("styles.min.css"))
   .pipe(autoprefixer({
      cascade: false
   }))
   .pipe(cleancss())
   .pipe(gulp.dest("./dist/css"))


const buildJs = () => gulp.src("./src/js/**/*.js")
   .pipe(concat("styles.min.js"))
   .pipe(terser())
   .pipe(gulp.dest("./dist/js"))

const minImages = () => gulp.src("./src/img/**/*")
   .pipe(imagemin())
   .pipe(gulp.dest("./dist/img"))

const cleanDist = () => gulp.src('./dist/*', { read: false }).pipe(clean());

export const build = gulp.series(cleanDist, gulp.parallel(minImages, buildStyles, buildJs,));

export const dev = gulp.series(build, () => {
   browserSync.init({
      server: {
         baseDir: "./"
      }
   })



   gulp.watch('./src/**/*', gulp.series(build, (done) => {
      BS.reload();
      done()
   }))
})