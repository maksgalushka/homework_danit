'use strict'


const burgerMenu = document.querySelector(".header__menu");
const burgerBtn = document.querySelector(".header__icon-burger");
let btnPossition = "active"

burgerBtn.addEventListener("click", (e) => {
   burgerMenu.classList.toggle("header__menu--active");
   if (btnPossition == "active") {
      burgerBtn.src = "./dist/img/menu-button-close.png"
      btnPossition = "passive"
   } else {
      burgerBtn.src = "./dist/img/menu-button.png"
      btnPossition = "active"
   }

})

