"use strict"

const buttonsList = document.querySelectorAll(".tabs>li");
const tabs = document.querySelectorAll(".tabs-content>li");


let currentText = 0;
let currentCategory = buttonsList[0].innerText;

const changeCurrentText = () => {
   document.querySelector(".active-paragraph")?.classList.remove("active-paragraph");
   document.querySelector(".active-tab")?.classList.remove("active-tab");
   for (let i = 0; i < tabs.length; i++) {
      if (currentCategory === tabs[i].dataset.category) {
         tabs[i].classList.add("active-paragraph");
         buttonsList[i].classList.add("active-tab");
      }
   }
}

buttonsList.forEach(btn => {
   btn.addEventListener('click', (event) => {
      currentCategory = btn.innerText;
      changeCurrentText()
   })
})


