"use strict"

const passwordForm = document.querySelector('.password-form');

passwordForm.addEventListener('mousedown', (e) => {
   if (e.target.classList.contains("fa-eye")) {
      e.target.style.display = "none";
      e.target.nextElementSibling.style.display = "inline";
      e.target.parentElement.querySelector("input").type = "login";
   }
})
passwordForm.addEventListener('mouseup', (e) => {
   if (e.target.classList.contains("fa-eye-slash")) {
      e.target.style.display = "none";
      e.target.previousElementSibling.style.display = "inline";
      e.target.parentElement.querySelector("input").type = "password";
   }
})

const password = document.querySelector("#password");
const repeatPassword = document.querySelector("#repeat-password");

passwordForm.addEventListener("submit", (e) => {
   if (repeatPassword.value !== password.value) {
      document.querySelector(".error").style.opacity = "100%";
   } else {
      document.querySelector(".error").style.opacity = "0";
      alert("You are welcome");
      e.target.reset();
   }

   e.preventDefault();
})


