"use strict"

// ?1. Почему для работы с input не рекомендуется использовать события клавиатуры?
// Потому что есть не один способ ввести информацию в инпуты, например: голосовой ввод или вставка с буфера.

const btnList = document.querySelectorAll(".btn")

document.addEventListener("keydown", (e) => {
   for (let i = 0; i < btnList.length; i++) {
      if (e.key.toLowerCase() === btnList[i].dataset.button.toLowerCase()) {
         document.querySelector(".active-btn")?.classList.remove("active-btn");
         btnList[i].classList.add("active-btn")
      }
   }
})