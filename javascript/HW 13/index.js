"use strict"
//? 1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
// setTimeout() - выполняется один раз.
// setInterval() - выполняется регулярно.
//? 2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// Функция будет выполнятся сразу же после выполнения кода.
//? 3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
// Потому что функция clearInterval() фоново нагружать систему.

const imgWrapper = document.querySelectorAll(".image-to-show");
const btnStop = document.querySelector(".end-animation");
const btnContinue = document.querySelector(".continue-animation");

let j = 1;

const animation = () => {
   document.querySelector(".current-img").classList.remove("current-img");
   console.log(j);
   imgWrapper[j].classList.add("current-img");
   j++;
   if (j > imgWrapper.length - 1) {
      j = 0;
   }
}

let slider = setInterval(animation, 3000);


btnStop.addEventListener("click", (e) => {
   clearInterval(slider);
   e.target.disabled = true;
   btnContinue.disabled = false;
})

btnContinue.addEventListener("click", (e) => {
   slider = setInterval(animation, 3000)
   e.target.disabled = true;
   btnStop.disabled = false;
})

