"use strict"

const ElementListFirst = document.querySelectorAll(".white-theme-first-color");
const ElementListSecond = document.querySelectorAll(".white-theme-second-color");
const switcher = document.querySelector(".hero-search")

const setWhiteColor = () => {
   for (let i = 0; i < ElementListFirst.length; i++) {
      ElementListFirst[i].classList.remove("dark-theme-first-color")
      ElementListFirst[i].classList.add("white-theme-first-color")
   }
   for (let i = 0; i < ElementListSecond.length; i++) {
      ElementListSecond[i].classList.remove("dark-theme-second-color")
      ElementListSecond[i].classList.add("white-theme-second-color")
   }
}

const setDarkColor = () => {
   for (let i = 0; i < ElementListFirst.length; i++) {
      ElementListFirst[i].classList.remove("white-theme-first-color")
      ElementListFirst[i].classList.add("dark-theme-first-color")
   }
   for (let i = 0; i < ElementListSecond.length; i++) {
      ElementListSecond[i].classList.remove("white-theme-second-color")
      ElementListSecond[i].classList.add("dark-theme-second-color")
   }
}





if (!localStorage.getItem('theme')) {
   localStorage.setItem('theme', 'white')
}

if (localStorage.getItem("theme") == "dark") {
   setDarkColor()
} else if (localStorage.getItem("theme") == "white") {
   setWhiteColor()
}

switcher.addEventListener("click", (e) => {
   if (localStorage.getItem("theme") == "white") {
      localStorage.setItem("theme", "dark");
      setDarkColor()
   } else if (localStorage.getItem("theme") == "dark") {
      localStorage.setItem("theme", "white");
      setWhiteColor()
   }
})




