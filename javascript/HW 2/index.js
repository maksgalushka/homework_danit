/*
!Вопрос 1
?Які існують типи даних у Javascript?

В JavaScript есть 8 основных типов.

   number - для любых чисел: целочисленных или чисел с плавающей точкой, целочисленные значения ограничены диапазоном ±2^53.

   bigint - для целых чисел произвольной длины.

   string - для строк. Строка может содержать один или больше символов, нет отдельного символьного типа.

   Boolean - для true/false.

   null - для неизвестных значений – отдельный тип, имеющий одно значение null.

   undefined - для неприсвоенных значений – отдельный тип, имеющий одно значение undefined.

   object - для более сложных структур данных.

   symbol - для уникальных идентификаторов.
*/

/*
!Вопрос 2
?У чому різниця між == і ===?

   == - это строгое равенство, проверяет равенство с не явным преоброзованием тиов.

   === - это строгое равенство, проверяет равенство без приведения типов.
*/
/*
!Вопрос 3
?Що таке оператор?

   Оператор — это специальный символ или выражение для проверки, изменения или сложения величин.
*/

let userName;
let userAge;

do {
   userName = prompt("Your name?");
} while(!isNaN(+userName));

do {
   userAge = +prompt("Your age?");
} while (isNaN(userAge));

console.log(userAge);
console.log(userName);

if (userAge < 18){

   alert("You are not allowed to visit this website.");

} else if(userAge >= 18 && userAge <= 22){

   let conforirmVerb = confirm("Are you sure you want to continue?");

   if (conforirmVerb){
      alert(`Welcome, ${userName}!`);
   } else{

      alert("You are not allowed to visit this website");
   }

} else{
   alert(`Welcome, ${userName}!`);
}