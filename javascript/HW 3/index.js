'use strict'
// ? 1. Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
/* 
	Циклы в программе помогают не повторять код несколько раз, упрощают читабельность программы а так же её размеры. 
*/
//? 2. Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.
/*
В JS существует 3 вида циклов for, while, do while.
	Цикл for используется когда нам известно какое колличество раз нужно повторить то или иное действие.
	Цикл while используется когда мы не знаем точно, какое колличество раз будет выполнятся действие.
	Цикл do while используется точно так же как и цикл while, но он должен выполнится МИНИМУМ один раз. 
*/
//? Що таке явне та неявне приведення (перетворення) типів даних у JS?
/*
	В JS существоет два вида приведения типов данных:
		ЯВНЫЙ - Когда мы сами меняем тип данных.
		НЕ ЯВНЫЙ - когда тип данных меняет сам JS.
*/

// ! MAIN TASK

let userMultipleNumber;

const getMultipleFive = function(userNumber) {
   if(userNumber < 5){
      console.log("Sorry, no numbers");
   } else{
      for (let i = 1; i < userNumber; i++) {
         if (i % 5 == 0) {
            console.log(i);
         } 
      }
   }

   
   
}

do{
   userMultipleNumber = +prompt('Enter your number');
} while(!Number.isInteger(userMultipleNumber));

getMultipleFive(userMultipleNumber);

//! ADDITIONAL TASK

//  function isPrime(num) {
// 	for (let i = 2; i < num; i++) {
// 		if (num % i === 0) {
// 			return false;
// 		}
// 	}
// 	return true;
// }

// function getPrimes(num1, num2) {
// 	for (let i = num1; i < num2; i++) {
// 		if (isPrime(i)) {
// 			console.log(i);
// 		}
// 	}
// }

// let m;
// let n;

// do {
//    m = +prompt("Enter first number");
//    n = +prompt("Enter second number");
// } while(isNaN(m) || isNaN(n) || m >= n);

// getPrimes(m, n);

