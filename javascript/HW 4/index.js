"use strict"

// ? Описати своїми словами навіщо потрібні функції у програмуванні
/* Функции в программировании нужны для того что бы не повторять код, с их помощью читать и воспринимать его гораздо легче. */
// ? Описати своїми словами, навіщо у функцію передавати аргумент.
/* Аргумент – это значение, которое передаётся функции при её вызове. Мы объявляем функции со списком параметров, затем вызываем их, передавая аргументы. */
// ? Що таке оператор return та як він працює всередині функції?
/* Оператор return возвращает значение функции и прекращает её работу. */



const getNumber = () => {
   let userNumber;


   do {
      userNumber = prompt("Введите число", userNumber);
   }
   while (isNaN(+userNumber) || userNumber === '');

   return +userNumber;
}

const getOperation = () => {
   let userChoice;

   do {
      userChoice = prompt("Введите одну из операций: '+', '-', '*', '/'", userChoice);
   }

   while (userChoice !== '+' && userChoice !== '-' && userChoice !== "*" && userChoice !== "/");

   return userChoice;
}

const sum = (num1, num2) => num1 + num2;

const subtraction = (num1, num2) => num1 - num2;

const multiple = (num1, num2) => num1 * num2;

const divison = (num1, num2) => num1 / num2;

const calculateArea = (userChoice, getNum1, getNum2) => {
   switch (userChoice) {
      case "+":
         return sum(getNum1, getNum2);
      case "-":
         return subtraction(getNum1, getNum2);
      case "*":
         return multiple(getNum1, getNum2);
      case "/":
         return divison(getNum1, getNum2);
   }
}


console.log(calculateArea(getOperation(), getNumber(), getNumber()))
