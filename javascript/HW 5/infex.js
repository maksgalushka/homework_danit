"use strict"

// ? 1. Опишіть своїми словами, що таке метод об'єкту
/* Метод объекта - это функция внутри объекта, в случае объекта и функция, и свойства, с которыми она работает - это часть одной большой переменной (объекта), поэтому функция может безопасно с ними работать, в отличии от обычной функции. */
// ? 2. Який тип даних може мати значення властивості об'єкта?
/*Любой тип данных*/
// ? 3. Об'єкт це посилальний тип даних. Що означає це поняття?
/*Это означает что нельзя на прямую скопировать объект, мы можем скопировать только ссылку на этот объект */

const createNewUser = () => {
   const newUser = {
      firstName: prompt("Enter your firstname."),
      lastName: prompt("Enter your lastname."),
      getLogin() {
         const userLogin = this.firstName[0] + this.lastName;
         return userLogin.toLowerCase()
      },
      setNewFirstName(newFName) {
         Object.defineProperty(newUser, "firstName", {
            writable: true,
         })
         this.firstName = newFName;
      },
      setNewLastName(newLName) {
         Object.defineProperty(newUser, "lastName", {
            writable: true,
         })
         this.lastName = newLName;
      },
   }
   Object.defineProperty(newUser, "firstName", {
      writable: false,
   });
   Object.defineProperty(newUser, "lastName", {
      writable: false,
   });

   return newUser
}

const myUser = createNewUser();

console.log(myUser.getLogin());

// myUser.firstName = "TEST 1";

// myUser.lastName = "TEST 2";


console.log(myUser);

myUser.setNewFirstName("TEST 1");

myUser.setNewLastName("TEST 2");


console.log(myUser);