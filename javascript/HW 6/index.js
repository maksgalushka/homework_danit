"use strict"

// ? 1.Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// Экронирование служит для того что бы системный символ сделать не системным.
// ? 2.Які засоби оголошення функцій ви знаєте?
// Существует три способа объявления функции: Function Declaration, Function Expression и Named Function Expression.
// ? 3.Що таке hoisting, як він працює для змінних та функцій?
// hoisting — это механизм в JavaScript, в котором переменные и объявления функций, передвигаются вверх своей области видимости перед тем, как код будет выполнен.

const createNewUser = () => {
   const newUser = {
      firstName: prompt("Enter your firstname."),
      lastName: prompt("Enter your lastname."),
      birthday: new Date(prompt("Enter your birthday", "dd.mm.yyyy")),
      getLogin() {
         const userLogin = this.firstName[0] + this.lastName;
         return userLogin.toLowerCase()
      },
      getAge() {
         return Math.floor((new Date - this.birthday) / 1000 / 60 / 60 / 24 / 365)
      },
      getPassword() {
         const userPassword = this.firstName[0].toLocaleUpperCase() + this.lastName + this.birthday.getFullYear();
         return userPassword
      }
   }

   return newUser
}

const myUser = createNewUser();

// console.log(new Date("2004.07.02"));

console.log(myUser);
console.log(myUser.getAge());
console.log(myUser.getPassword())
