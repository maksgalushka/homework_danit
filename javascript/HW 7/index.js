"use strict"

// ? 1.Опишіть своїми словами як працює метод forEach.
// Метод forEach позволяет запускать функцию для каждого элемента в массиве.
// ? 2.Як очистити масив?
// Очистить массив можно задав его длину 0.
// ? 3.Як можна перевірити, що та чи інша змінна є масивом?
// с помощью метода isArray.

const myArray = [123, "text", { a: 1 }, function () { }, true, [1, 2]];
const allTypes = ['string', 'number', 'boolean', 'object', 'function'];

const filterBy = (arr, excludedTypeof) => arr.filter(item => (typeof (item) !== excludedTypeof));

allTypes.forEach(item => console.log(filterBy(myArray, item)));

