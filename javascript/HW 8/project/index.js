"use strict"
// ? 1.Опишіть своїми словами що таке Document Object Model (DOM)
// OBM - это модель документа, которая представляет все содержимое страницы в виде объектов, которые можно менять.
// ? 2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// innerHTML - выводит содержимое тега и сам тег.
// innerText - выводит только содержимое тега.
// ? 3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// Можно использовать два способа: 
//  querySelector или querySelectorAll - более универсальный метод.
//  getElementBy* Встречается в старом коде, полезный в отдельных случаях.


const paragraphColection = document.querySelectorAll('p');

paragraphColection.forEach((paragraph) => {
   paragraph.style.backgroundColor = "#ff0000";
})

const optionListElem = document.querySelector("#optionsList");

console.log(optionListElem);
console.log(optionListElem.parentElement);
console.log(optionListElem.childNodes);

const testParagraphElem = document.querySelectorAll('#testParagraph');

testParagraphElem.forEach((paragraph) => {
   paragraph.innerHTML = "This is a paragraph"
})

const mainHeaderElem = document.querySelector(".main-header")

console.log(mainHeaderElem.children)

for (const item of mainHeaderElem.children) {
   item.classList.add("nav-item")
}

const sectionTitleElem = document.querySelectorAll(".section-title")


sectionTitleElem.forEach((item) => {
   item.classList.remove("section-title")
})
