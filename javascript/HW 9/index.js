"use strict"
// ? 1. Опишіть, як можна створити новий HTML тег на сторінці.
// document.createElement("Тег")
// ? 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// Первый параметр функции - это то место куда мы будем вставлять HTML код
// 'beforebegin': до самого element (до открывающего тега).
// 'afterbegin': сразу после открывающего тега  element (перед первым потомком).
// 'beforeend': сразу перед закрывающим тегом element (после последнего потомка).
// 'afterend': после element (после закрывающего тега).
// ? 3. Як можна видалити елемент зі сторінки?
// методом .remove()
const arrayList = ["Kharkiv", "Kiev", ["Borispol", "Irpin", ["Zhytomyr", "Poltava"]], "Odessa", "Lviv", "Dnieper"];


const setList = (array, element = document.body) => {
   const ul = document.createElement('ul');
   ul.classList.add('list');

   array.forEach(item => {
      const li = document.createElement('li');
      li.classList.add('list-item');

      if (typeof (item) !== 'object') {
         li.innerHTML = item;
         ul.insertAdjacentElement("beforeend", li)
      } else {
         setList(item, li)
         ul.insertAdjacentElement("beforeend", li)
      }
   })

   element.insertAdjacentElement("beforeend", ul)
}

setList(arrayList);